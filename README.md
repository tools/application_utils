[![pipeline status](https://gitlab.windenergy.dtu.dk/tools/application_utils/badges/master/pipeline.svg)](https://gitlab.windenergy.dtu.dk/tools/application_utils/commits/master)
[![coverage report](https://gitlab.windenergy.dtu.dk/tools/application_utils/badges/master/coverage.svg)](https://gitlab.windenergy.dtu.dk/tools/application_utils/commits/master)


Install
=======

```
conda install -c conda-forge qscintilla2
```

```
pip install -e .
```