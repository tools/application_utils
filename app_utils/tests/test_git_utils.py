"""test functions in version_utils"""


import time
import pytest
from app_utils import version
from app_utils.utils import git_utils
import app_utils
import os
from app_utils.tests.test_main import run_module_main
import importlib
git_path = os.path.dirname(app_utils.__file__) + "/../"


def test_update_git_version():
    """test update git version"""
    with open(version.__file__, 'w') as fid:
        fid.write("__version__ = 'missing'")
    importlib.reload(version)
    print(version.__version__)
    assert version.__version__ == "missing"
    git_utils.set_version_from_git(app_utils, git_path)
    time.sleep(0.1)
    importlib.reload(version)
    importlib.reload(version)

    print(version.__version__)
    assert version.__version__ != "missing"


# def test_not_git_repo():
#     """test update_git_version on module not in git root"""
#     none_git_repo_path = os.path.dirname(git_utils.__file__).replace("\\", "/")
#     with pytest.raises(Warning, match=r"'%s' does not appear to be a Git repository" % none_git_repo_path):
#         git_utils.set_version_from_git(app_utils, none_git_repo_path)


def test_git_version():
    print(git_utils.get_git_version(git_path))


def test_tag():
    print(git_utils.get_tag(git_path))


def test_main():
    run_module_main(git_utils)
