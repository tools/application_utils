import subprocess
import sys


def test_pycodestyle():
    try:
        s = subprocess.run(
            f'{sys.executable} -m pycodestyle --ignore=E501,W504,E741 ./app_utils'.split(),
            capture_output=True)
        assert s.returncode == 0
    except AssertionError as e:
        print(s.stdout.decode())
        raise e
