from app_utils.qt_gui_loader._qt_gui_loader import QtMainWindowLoader
from app_utils.ui.qt_progress_information import QtProgressInformation
import time
import pytest
import os


def test_progress_iterator():
    class MW(QtMainWindowLoader):
        def __init__(self, ui_module=None, connect_actions=True, **kwargs):
            QtMainWindowLoader.__init__(self, ui_module=ui_module, connect_actions=connect_actions, **kwargs)
            self.progress_iterator = QtProgressInformation(self).progress_iterator

        def post_initialize(self):
            for _ in self.progress_iterator(range(100), "progressbar(without cancel)", False):
                time.sleep(.001)
            self.terminate()

    with pytest.raises(SystemExit, match="0"):
        MW().start()
