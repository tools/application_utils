import os
import shutil
from app_utils import build_exe
import pytest
from app_utils.tests.test_files import version
from app_utils.functions.process_exec import pexec
from app_utils.build_exe import make_standard_installer
import importlib
from app_utils.tests.test_main import run_module_main
from app_utils.tests.test_files import my_program


@pytest.fixture(autouse=True)
def run_around_tests():
    clean()
    yield
    clean()


def clean():
    shutil.rmtree(os.path.dirname(build_exe.__file__) + "/dists", ignore_errors=True)


def test_build_exe_main():
    run_module_main(make_standard_installer)

    importlib.reload(version)
    fn = 'my_program-%s-%s' % (my_program.__version__, make_standard_installer.get_exe_platform())
    assert os.system(("./dists/%s/my_program/my_program terminate" % fn).replace("/", os.path.sep)) == 0
    err_code, stdout, stderr, cmd = pexec([("./dists/%s/my_program/%s/my_program" %
                                            (fn, fn)).replace("/", os.path.sep), 'terminate'])
    assert err_code == 0
    importlib.reload(version)
    assert stdout.strip().startswith("Hello world")
    assert stderr.strip() == ""


#     def test_pyqt4(self):
#         if os.path.isdir("demonstration/pyqt_window_dist"):
#             shutil.rmtree("demonstration/pyqt_window_dist/")
#         build_cx_exe.build_exe('demonstration/pyqt_window.py', "2.0.0", modules=[PYQT4, MATPLOTLIB, NUMPY], include_files=['DTU_logo.png'])
#         #self.assertTrue(os.path.isfile("demonstration\\pyqt_window_dist\\exe.win-amd64-3.3\\pyqt_window.exe"))
#         #os.system("demonstration\\pyqt_window_dist\\exe.win-amd64-3.3\\pyqt_window.exe")

#    def test_pyqt4_2(self):
#        if os.path.isdir("demonstration/pyqt_window_dist"):
#            shutil.rmtree("demonstration/pyqt_window_dist/")
#        build_cx_exe.build_exe('demonstration/pyqt_window.py', "2.0.0", modules=[PYQT4, MATPLOTLIB, NUMPY], icon='demonstration/pydap.ico')
#        self.assertTrue(os.path.isfile("demonstration\\pyqt_window_dist\\exe.win-amd64-3.3\\pyqt_window.exe"))
#        os.system("demonstration\\pyqt_window_dist\\exe.win-amd64-3.3\\pyqt_window.exe")

#    def test_hdf5_exe(self):
#        if os.path.isdir("my_program_dist"):
#            shutil.rmtree("my_program_dist/")
#        import pandas
#
#        build_cx_exe.build_exe('my_program.py', "2.0.0", modules=[NUMPY, HDF5])
#        self.assertTrue(os.path.isfile("my_program_dist/exe.win-amd64-3.3/my_program.exe"))
#        print ("finish")
