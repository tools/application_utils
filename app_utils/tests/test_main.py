"""Test main functions in all modules"""
import os
import importlib
import pkgutil
import sys
import warnings

from unittest import mock
import pytest
import matplotlib.pyplot as plt

import app_utils


def get_main_modules():
    """return a list of all modules with main function"""
    package = app_utils
    modules = []
    for _, modname, _ in pkgutil.walk_packages(package.__path__, package.__name__ + '.'):
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            module = importlib.import_module(modname)

        if 'main' in dir(module):
            modules.append(module)
    return modules


def print_main_modules():
    """print list of all modules with main function"""
    print("\n".join([m.__name__ for m in get_main_modules()]))


def run_module_main(module):
    """Run the main functions"""
    # check that all main module examples run without errors
#     if os.name == 'posix' and "DISPLAY" not in os.environ:
#         pytest.xfail("No display")

    def no_show(*_):
        pass
    plt.show = no_show  # disable plt show that requires the user to close the plot

    def no_print(*_):
        pass
    if 'terminate' not in sys.argv:
        sys.argv.append('terminate')

    try:
        print(module)
        with mock.patch.object(module, "__name__", "__main__"):
            with mock.patch.object(module, "print", no_print):
                getattr(module, 'main')()
    except Exception as ex:
        raise type(ex)(str(ex) +
                       ' in %s.main' % module.__name__).with_traceback(sys.exc_info()[2])


if __name__ == '__main__':
    print_main_modules()
