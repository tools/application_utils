try:
    from . import version
    __version__ = version.__version__
except Exception:
    pass

if __name__ == '__main__':
    import sys
    print('Hello world')
    print("\n".join(sys.argv))
    if sys.argv[-1] != 'terminate':
        input("Press any key")
