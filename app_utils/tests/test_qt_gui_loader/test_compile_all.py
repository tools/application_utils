from app_utils.qt_gui_loader import compile_all
from app_utils.qt_gui_loader.examples.ui import MyWidgetUI
from app_utils.tests.test_main import run_module_main


def test_compile_all():

    with open(MyWidgetUI.__file__, 'r') as fid:
        before = fid.read()
    with open(MyWidgetUI.__file__, 'w') as fid:
        pass

    run_module_main(compile_all)

    with open(MyWidgetUI.__file__, 'w', newline='\n') as fid:
        fid.write(before.replace("\r\n", '\n'))
