import pytest

from app_utils.qt_gui_loader._qt_gui_loader import QtMainWindowLoader
from app_utils.qt_gui_loader.examples.ui import MyWidgetUI
from app_utils.qt_gui_loader.examples import MyEmptyMainWindow, MyMainWindow
import sys
from unittest import mock
import os


def test_main_window():
    class MyMainWindow(QtMainWindowLoader):
        def __init__(self, parent=None):
            ui_module = MyWidgetUI
            try:
                self.ui = ui_module.Ui_Form()  # enable autocomplete
            except Exception:
                pass
            QtMainWindowLoader.__init__(self, ui_module)
            self.ui.lineEdit.setText("MyMainWindow")
            self.setWindowTitle("MyMainWindow")

        def actionPrintText(self):
            print("Mainwindow text: %s" % self.ui.lineEdit.text())

        def post_initialize(self):
            QtMainWindowLoader.post_initialize(self)
            self.actionPrintText()
            self.terminate()

    with pytest.raises(SystemExit, match="0"):
        MyMainWindow().start()
    print("here MyMainWindow")


@pytest.mark.parametrize('examples', [MyMainWindow,
                                      MyEmptyMainWindow,
                                      # MyDialog,
                                      # MyWidgetWindow
                                      ])
def test_dialog_examples(examples):
    module = examples

    def no_print(_):
        pass
    if 'terminate' not in sys.argv:
        sys.argv.append('terminate')

    try:
        print(module)
        with mock.patch.object(module, "__name__", "__main__"):
            with mock.patch.object(module, "print", no_print):
                with pytest.raises(SystemExit, match="0"):
                    getattr(module, 'run_example')()
    except Exception as ex:
        raise type(ex)(str(ex) +
                       ' in %s.main' % module.__name__).with_traceback(sys.exc_info()[2])
