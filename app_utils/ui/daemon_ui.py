import sys

from app_utils.ui import OutputUI, StatusUI, UI


class DaemonOutputUI(OutputUI, StatusUI):
    def __init__(self, parent=None):
        OutputUI.__init__(self)
        StatusUI.__init__(self)

    def show_error(self, msg, title="Error"):
        if isinstance(msg, Exception):
            title = msg.__class__.__name__
            msg = str(msg)
        sys.stderr.write("%s\n%s" % (title, msg))


class DaemonUI(DaemonOutputUI, UI):
    pass
