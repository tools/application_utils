import sys

from qtpy.QtWidgets import QApplication, QDialog, QLabel, QLineEdit, QCheckBox, \
    QPushButton, QHBoxLayout, QVBoxLayout, QAction
from qtpy.QtCore import Signal, Qt


class FindDialog(QDialog):
    find = Signal(str)
    findPrevious = Signal(str, int)
    find_next = Signal()
    find_previous = Signal()

    def __init__(self, *args, **kwargs):
        super(FindDialog, self).__init__(*args, **kwargs)
        label = QLabel(self.tr('&Search for:'))
        self.lineEdit = QLineEdit()
        label.setBuddy(self.lineEdit)

        self.wholewordCheckBox = QCheckBox(self.tr('Match &whole word'))
        self.caseCheckBox = QCheckBox(self.tr('&Match case'))

        self.findButton = QPushButton(self.tr("&Find"))
        self.findButton.setDefault(True)
        self.findButton.setEnabled(False)

        closeButton = QPushButton(self.tr('&Close'))

        self.lineEdit.textChanged.connect(self.enableFindButton)
        self.findButton.clicked.connect(self.findClicked)
        closeButton.clicked.connect(self.close)

        topLeftLayout = QHBoxLayout()
        topLeftLayout.addWidget(label)
        topLeftLayout.addWidget(self.lineEdit)

        leftLayout = QVBoxLayout()
        leftLayout.addLayout(topLeftLayout)
        leftLayout.addWidget(self.caseCheckBox)
        leftLayout.addWidget(self.wholewordCheckBox)

        rightLayout = QVBoxLayout()
        rightLayout.addWidget(self.findButton)
        rightLayout.addWidget(closeButton)
        rightLayout.addStretch()

        mainLayout = QHBoxLayout()
        mainLayout.addLayout(leftLayout)
        mainLayout.addLayout(rightLayout)
        self.setLayout(mainLayout)

        self.setWindowTitle(self.tr("Find"))
        self.setFixedHeight(self.sizeHint().height())

        self.addAction(QAction("Find next", self, shortcut=Qt.Key_F3, triggered=self.find_next.emit))
        self.addAction(QAction("Find previous", self, shortcut='Shift+F3', triggered=self.find_previous.emit))

    def enableFindButton(self, text):
        self.findButton.setEnabled(bool(text))

    def findClicked(self):
        self.find.emit(self.lineEdit.text())

    def getFindText(self):
        return self.lineEdit.text()

    def isCaseSensitive(self):
        return self.caseCheckBox.isChecked()

    def isWholeWord(self):
        return self.caseCheckBox.isChecked()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    d = FindDialog
    d.show()
    app.exec()
