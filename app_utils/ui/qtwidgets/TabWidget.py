import sys
from qtpy import QtCore, QtWidgets
from qtpy.QtWidgets import QTabWidget, QLabel, QWidget, QTabBar, QMenu


class TabWidget(QTabWidget):

    def __init__(self, tab_contents_widget, default_tab_name="New"):
        QTabWidget.__init__(self)
        self.tab_contents_widget = tab_contents_widget
        self.default_tab_name = default_tab_name
        self.setTabsClosable(True)
        self.setMovable(True)
        self.insertTab(0, QWidget(), "")
        self.new_label = QLabel("*")
        self.tabBar().setTabButton(0, QTabBar.RightSide, self.new_label)
        self.currentChanged.connect(self.current_tab_changed)
        self.tabCloseRequested.connect(self.close_tab)
        self.tabBar().tabMoved.connect(self.tabMoved)

        self.labels = lambda: [str(self.tabBar().tabText(i)).lower() for i in range(self.count())]
        self.tabBar().setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.tabBar().customContextMenuRequested.connect(self.openMenu)
        self.add_tab()

    def widget(self, tab=None):
        """tab must be index, text, widget, tab or None(current tab)"""
        if tab is None:
            tab = self.currentWidget()
        if isinstance(tab, int):
            tab = self.tabText(tab)
        tab = str(tab)
        if tab.lower() not in self.labels():
            self.add_tab(tab)
        index = self.labels().index(str(tab).lower())
        return QTabWidget.widget(self, index)

    def widgets(self):
        return [self.widget(i) for i in range(self.count() - 1)]

    def current_tab_changed(self, index):
        # Add new tab if "*" tab is clicked
        if index == self.count() - 1:
            self.add_tab()
        else:
            self.widget(index).update()

    def close_tab(self, index):
        self.setCurrentIndex(index - 1)
        try:
            self.widget(index).terminate()
        except Exception:
            pass
        self.removeTab(index)

    def add_tab(self, label=None):
        widget = self.tab_contents_widget()
        if label is None:
            label = widget.name
        # Do not set tab label here in order to avoid duplicate labels
        index = self.insertTab(self.count() - 1, widget, u"")

        actual_name = self.setTabText(index, label)
        widget.name = actual_name
        self.setCurrentIndex(index)
        return index

    def setTabText(self, index, tab_text):
        tab_text = self.unique_tab_text(tab_text, index)
        QTabWidget.setTabText(self, index, tab_text)
        return tab_text

    def unique_tab_text(self, tab_text, index=-1):
        labels = self.labels()
        i = 0
        lbl = tab_text
        while lbl.lower() in labels and labels.index(lbl.lower()) != index or lbl == self.default_tab_name:
            i += 1
            lbl = "%s%d" % (tab_text, i)
        return lbl

    def tabMoved(self, from_index, to_index):
        if from_index == self.count() - 1:
            self.tabBar().moveTab(to_index, from_index)

    def openMenu(self, position):
        def edit():
            menu = QMenu()
            editAction = menu.addAction("Rename")
            return editAction == menu.exec_(self.mapToGlobal(position))
        tabindex = self.tabBar().tabAt(position)
        if tabindex < self.count() - 1:
            self.setCurrentIndex(tabindex)

            if edit():
                tabname, ok = QtWidgets.QInputDialog.getText(
                    self, "Enter new name", "New name", text=self.tabText(tabindex))
                if ok:
                    self.widget(tabindex).name = tabname
                    self.setTabText(tabindex, str(tabname))

    def close_all_tabs(self):
        for _ in range(self.count() - 1):
            self.close_tab(0)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)

    def ptt(txt):
        print(txt)
    m = QtWidgets.QMainWindow()

    def new_widget():
        return QtWidgets.QPushButton("hello")
    t = TabWidget(new_widget)

    m.setCentralWidget(t)
    t.connect
    m.show()
    sys.exit(app.exec_())
