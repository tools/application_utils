
import os
import sys
import traceback

from qtpy.QtCore import Qt
from qtpy.QtGui import QCursor
from qtpy.QtWidgets import QApplication, QCheckBox, QLineEdit, QDialog, \
    QPushButton, QFormLayout, QMainWindow, QLabel
from qtpy.QtWidgets import QMessageBox, QFileDialog, QInputDialog
from qtpy.QtWidgets import QWidget

from app_utils.ui import OutputUI, InputUI, StatusUI
from app_utils.ui.qt_progress_information import QtProgressInformation
from app_utils.ui.qt_ui_base import QtUIBase
import inspect
from PyQt5.QtWidgets import QGridLayout


class QtStdOut():
    def __init__(self, QtOutput):
        self.qtOutputUI = QtOutput
        self.txt = ""

    def flush(self):
        if self.txt:
            pass  # self.qtOutputUI.show_message(self.txt)
        self.txt = ""

    def write(self, txt):
        self.txt += txt


class QtStdErr(QtStdOut):

    def flush(self):
        pass

    def write(self, txt):
        if txt:
            self.qtOutputUI.show_error(txt, show_traceback=True)


class QtOutputUI(QtUIBase, OutputUI):
    show_traceback = False

    def __init__(self, parent, **kwargs):
        super().__init__(parent, **kwargs)
        self.stdout = QtStdOut(self)
        self.stderr = QtStdErr(self)

    def _show_box(self, box_func):
        cursor = QApplication.overrideCursor()
        QApplication.restoreOverrideCursor()
        box_func()
        if cursor and isinstance(cursor, QCursor) and cursor.shape() == Qt.WaitCursor:
            QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        QApplication.processEvents()

    def show_message(self, msg, title="Information"):
        self._show_box(lambda: QMessageBox.information(self.parent, title, msg))

    def show_warning(self, msg, title="Warning"):
        """Show a warning dialog box
        msg: Error message or Warning object
        """
        if isinstance(msg, Warning):
            title = msg.__class__.__name__
            msg = str(msg)

        self._show_box(lambda: QMessageBox.warning(self.parent, title, msg))

    def show_error(self, msg, title="Error", show_traceback=None):
        """Show a warning dialog box
        msg: Error message or Exception object
        """
        if show_traceback or self.show_traceback:
            if isinstance(msg, Exception):
                tb = "".join(traceback.format_tb(msg.__traceback__))
                # tb = "".join(traceback.format_exception(msg)) # only python>=3.10
            else:
                tb = traceback.format_exc()
        if isinstance(msg, Exception):
            e = msg
            title = e.__class__.__name__
            msg = "\n".join(e.args)
        if show_traceback or self.show_traceback:
            msg += "\n" + "\n".join([l for l in tb.split("\n") if '<frozen importlib._bootstrap>' not in l])
        parent = self.parent
        if not isinstance(parent, (QWidget, type(None))) and isinstance(parent(), (QWidget, type(None))):
            parent = parent()
        msg = msg.replace("\\n", '<br>')
        self._show_box(lambda parent=parent, title=title, msg=msg: QMessageBox.critical(parent, title, msg))

    def show_text(self, text, end="\n", flush=False):
        print(text, end=end)
        if flush:
            sys.stdout.flush()


class QtInputUI(QtUIBase, InputUI):
    def __init__(self, parent, **kwargs):
        super().__init__(parent, **kwargs)
        if QApplication.instance() is None:
            self.app = QApplication(sys.argv)  # For unit testing

    def get_confirmation(self, title='Confirm', msg='', allow_cancel=False):
        if allow_cancel:
            res = QMessageBox.question(self.parent, title, msg, QMessageBox.Yes |
                                       QMessageBox.No | QMessageBox.Cancel, QMessageBox.Yes)
            return {QMessageBox.No: False, QMessageBox.Yes: True, QMessageBox.Cancel: None}[res]
        else:
            return QMessageBox.Yes == QMessageBox.question(
                self.parent, title, msg, QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)

    def get_confirmation_dontaskagain(self, title='Confirm', msg='Please confirm'):
        class DialogWithCheckBox(QMessageBox):
            def __init__(self, parent, title, msg):
                super(DialogWithCheckBox, self).__init__()
                self.checkbox = QCheckBox()
                self.setWindowTitle(title)
                self.setText(msg)
                self.checkbox.setText("Remember my decision")
                self.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
                self.setDefaultButton(QMessageBox.Yes)
                self.setIcon(QMessageBox.Question)
                self.setCheckBox(self.checkbox)

            def exec_(self, *args, **kwargs):
                """
                Override the exec_ method so you can return the value of the checkbox
                """
                return QMessageBox.exec_(self, *args, **kwargs), self.checkbox.isChecked()
        answer, dontaskagain = DialogWithCheckBox(self.parent, title, msg).exec_()
        return QMessageBox.Yes == answer, dontaskagain

    def get_string(self, title='', msg='', default=""):
        """
        returns
        -------
        text : str
            String
        ok : boolean
            if ok: True
            if cancel: False

        Examples
        --------
        text, ok = get_string("The Title", "The meassage", "Default value")
        if ok:
            print (text)
        else:
            print ("cancel")
        """
        return QInputDialog.getText(self.parent, title, msg, text=default)

    def get_username(self, msg="Username"):
        username, ok = QInputDialog.getText(None, "Enter username", msg)
        if not ok:
            username = ""
        return str(username)

    def get_password(self, msg="Password"):
        password, ok = QInputDialog.getText(None, "Enter password", msg, QLineEdit.Password)
        if not ok:
            password = ""
        return str(password)

    def get_login(self, username="", title="Log in"):
        login = Login(username, title)
        login.exec_()
        return str(login.textName.text()), str(login.textPass.text())

    def get_hostlogin(self, host="", username="", title="Log in"):
        login = HostLogin(host, username, title)
        login.exec_()
        return str(login.textHost.text()), str(login.textName.text()), str(login.textPass.text())

    def get_xxx_name(self, diaglog_func, title, filetype_filter=None,
                     file_dir=None, selected_filter=None, default_id=None):
        if default_id is None:
            stack_function_names = [v.function for v in inspect.stack() if v.function not in {
                '<module>', 'main', 'run', '_exec', 'execfile', 'wrapper', 'safe_function', '<lambda>'}]
            default_id = stack_function_names[stack_function_names.index('get_xxx_name') + 2]

        if (file_dir is None or os.path.dirname(file_dir) == "") and hasattr(self, 'load_setting'):
            default_dir = self.load_setting(f'default_dir{default_id}', '.')
            if os.path.isdir(default_dir):
                file_dir = default_dir
            else:
                file_dir = ""

        if selected_filter:
            filetype_filter = ";;".join((selected_filter, filetype_filter))
        r = diaglog_func(self.parent, title, file_dir,
                         *[v for v in [filetype_filter, selected_filter] if v is not None])
        if isinstance(r, tuple):
            r = r[0]
        if isinstance(r, list):
            r = [r_.replace('\\', '/') for r_ in r]
            if r and r[0] != "":
                self.save_setting(f'default_dir{default_id}', os.path.dirname(r[0]))
        else:
            r = r.replace('\\', '/')
            if r != "":
                self.save_setting(f'default_dir{default_id}', os.path.dirname(r))
        return r

    def get_open_filename(self, title="Open", filetype_filter="*.*", file_dir=None, selected_filter=None):
        """
        fn = gui.get_open_filename(title="Open", filetype_filter="*.dit1;*dit2;;*.dat", file_dir=".", selected_filter="*.dat")
        if fn == "": return #Cancel
        """
        return self.get_xxx_name(QFileDialog.getOpenFileName, title, filetype_filter,
                                 file_dir, selected_filter)

    def get_save_filename(self, title='Save', filetype_filter='*.*', file_dir=None, selected_filter=""):
        """
        fn = gui.get_save_filename(title="title", filetype_filter="*.dit1;*dit2;;*.dat", file_dir=".", selected_filter="*.dat")
        if fn == "": return #cancel
        """
        return self.get_xxx_name(QFileDialog.getSaveFileName, title, filetype_filter,
                                 file_dir, selected_filter)

    def get_open_filenames(self, title='Open', filetype_filter="*.*", file_dir=None, selected_filter=""):
        """
        fn = gui.get_open_filenames(title="title", filetype_filter="*.dit1;*dit2;;*.dat", file_dir=".")
        if fn == []: return #cancel
        """
        return self.get_xxx_name(QFileDialog.getOpenFileNames, title, filetype_filter,
                                 file_dir, selected_filter)

    def get_foldername(self, title='Select folder', file_dir=None):
        return self.get_xxx_name(QFileDialog.getExistingDirectory, title=title, file_dir=file_dir)


class Login(QDialog):
    def __init__(self, username="", title="Log on as"):
        QDialog.__init__(self)
        self.setWindowTitle(title)
        self.textName = QLineEdit(username, self)
        self.textPass = QLineEdit(self)
        self.textPass.setEchoMode(QLineEdit.Password)
        self.buttonLogin = QPushButton('Login', self)
        self.buttonLogin.clicked.connect(self.accept)
        layout = QFormLayout(self)
        layout.addRow(QLabel("Username:"), self.textName)
        layout.addRow(QLabel("Password:"), self.textPass)
        layout.addWidget(self.buttonLogin)


class HostLogin(QDialog):
    def __init__(self, host="", username="", title='Host connection'):
        QDialog.__init__(self)
        self.setWindowTitle(title)
        self.textHost = QLineEdit(host, self)
        self.textName = QLineEdit(username, self)
        self.textPass = QLineEdit(self)
        self.textPass.setEchoMode(QLineEdit.Password)
        self.buttonLogin = QPushButton('Login', self)
        self.buttonLogin.clicked.connect(self.accept)
        layout = QFormLayout(self)
        layout.addRow(QLabel("Host:"), self.textHost)
        layout.addRow(QLabel("Username:"), self.textName)
        layout.addRow(QLabel("Password:"), self.textPass)
        layout.addWidget(self.buttonLogin)


class QtStatusUI(QtProgressInformation, QtUIBase, StatusUI):

    def __init__(self, parent, **kwargs):
        super().__init__(parent=parent, **kwargs)

    def _start_wait(self):
        """Changes mouse icon to waitcursor"""
        StatusUI._start_wait(self)
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))

    def _end_wait(self):
        """Restores default mouse icon"""
        StatusUI._end_wait(self)
        QApplication.restoreOverrideCursor()


class QtUI(QtOutputUI, QtInputUI, QtStatusUI):
    def __init__(self, parent=None, **kwargs):
        if parent is None:
            if QApplication.startingUp():
                self.app = QApplication(sys.argv)
            else:
                self.app = QApplication.instance()
        super().__init__(parent=parent, **kwargs)


if __name__ == "__main__":
    from PyQt5 import QtWidgets
    from app_utils.qt_gui_loader._qt_gui_loader import QtMainWindowLoader, QtWidgetLoader

    class MW(QWidget):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            # QtUI.__init__(self, self)
            vbox = QtWidgets.QVBoxLayout()
            gui = self.parent()
            for n in [m[0] for m in inspect.getmembers(QtInputUI, inspect.isfunction)
                      if m[0].startswith('get') and m[0] != 'get_xxx_name']:

                #     for n in [('Get confirmation', gui.get_confirmation()),
                #           ('Open file', 'Open folder', 'Open files', 'Save file', 'Progress iterator']:
                button = QPushButton(n)

                def f(r, n=n):
                    print(getattr(gui, n)())
                button.clicked.connect(f)
                vbox.addWidget(button)
            self.setLayout(vbox)
            gui.setCentralWidget(self)

        def openfile(self, *_):
            print(self.gui.get_open_filename())

        def openfolder(self, *_):
            print(self.gui.get_foldername())

        def openfiles(self, *_):
            print(self.gui.get_open_filenames())

        def savefile(self, *_):
            print(self.gui.get_save_filename())

        def progressiterator(self, *args, **kwargs):
            import time
            try:
                with self.gui.wait_cursor():
                    for _ in self.gui.progress_iterator(range(10)):
                        time.sleep(0.1)
            except Exception:
                sys.stderr.write(traceback.format_exc())

    window = QtMainWindowLoader()
    w = MW(window)
    window.start()

    # print (mw.get_confirmation_dontaskagain("title", "msg"))

    # sys.exit(app.exec_())
