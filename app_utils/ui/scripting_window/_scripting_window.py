from os import makedirs
import os
import sys
import time
import traceback
#
# from pywin.scintilla.find import FindDialog
# from qtpy.QtCore import Qt
# from qtpy.QtGui import QIcon
# from qtpy.QtWidgets import QApplication, QAction, QTextBrowser
#
# from app_utils.qt_gui_loader._qt_gui_loader import QtMainWindowLoader,\
#     QtDialogLoader
# from app_utils.ui.scripting_window import ScriptingWindowUI
# from app_utils.ui.scripting_window.script_editor.qscintilla2_script_editor import Qscintilla2ScriptEditor
# from app_utils.ui.scripting_window.script_runner import ScriptRunner
# from app_utils.ui.qt_ui import QtUI
# from app_utils.ui.qtwidgets.TabWidget import TabWidget


from app_utils.qt_gui_loader import QtMainWindowLoader, QtDialogLoader
from app_utils.ui.qt_ui import QtUI

from app_utils.ui.scripting_window import ScriptingWindowUI
# from os import makedirs
# import os
# import sys
# import time
# import traceback

from qtpy.QtGui import QIcon
from qtpy.QtWidgets import QApplication, QAction, QTextBrowser
from qtpy.QtCore import Qt
from app_utils.ui.scripting_window.script_editor.qscintilla2_script_editor import Qscintilla2ScriptEditor
from app_utils.ui.scripting_window.script_runner import ScriptRunner
from app_utils.ui.qtwidgets.find_dialog import FindDialog
from app_utils.ui.qtwidgets.TabWidget import TabWidget


def beep(frq=2500, dur=50):
    try:
        import winsound
        winsound.Beep(frq, dur)
    except Exception:
        pass


class ScriptingWindow():
    autosave_splitter = """
#===============================================================================
# Autosave
#===============================================================================
"""
    autosave_warning_shown = False

    def __init__(self, scriptEditorClass, scriptRunner, **kwargs):
        super().__init__(**kwargs)
        self.scriptEditor = scriptEditorClass(self.ui.verticalLayoutWidget)
        self.ui.verticalLayout_2.addWidget(self.scriptEditor)
        self.ui.splitter_2.setSizes([1000, 0])
        self.scriptEditor.title_changed.connect(self.setWindowTitle)

        self.scriptRunner = scriptRunner
        scriptRunner.scriptingWindow = self
        scriptRunner.scriptingWindow = self
        self.scriptEditor.get_autocomplete_list = self.scriptRunner.get_autocomplete_list

        self.autosave_filename = self.load_setting(
            'autosave_filename', os.path.expanduser('~/.%s/autosave.py' % self.app_name))

        self.actionNew(*self.load_autosave()[0])

        self.scriptEditor.setFocus()
        self.scriptEditors = [self.scriptEditor]

        # self.setAcceptDrops(True)???
        # self.setFocusPolicy(Qt.StrongFocus)???

    def _actionSet_autosave_file(self):
        fn = self.get_save_filename('Save as...', 'autosave.py;;*.py', self.autosave_filename)
        if fn == "":
            return  # Cancel
        self.autosave_filename = fn
        self.save_setting('autosave_filename', self.autosave_filename)
        self.autosave()

    def autosave(self):
        s = []
        for se in self.scriptEditors:
            s.append("%s#%s\n#%s\n%s" % (self.autosave_splitter,
                                         se.filename or se.name,
                                         se.file_sync_time,
                                         se.script))
        try:
            makedirs(os.path.dirname(self.autosave_filename), exist_ok=True)
            with open(self.autosave_filename, 'w') as fid:
                fid.write("".join(s))
        except IOError:
            if self.autosave_warning_shown is False:
                self.autosave_warning_shown = True
                self.gui.show_warning("Temporary scripts could not be saved automatically\n\nPermission denied to: '%s'" %
                                      os.path.realpath(self.autosave_filename))

    def load_autosave(self):
        try:
            if os.path.isfile(self.autosave_filename):
                with open(self.autosave_filename) as f:
                    autosave = f.read()
                scripts = autosave.split(self.autosave_splitter)
            else:
                scripts = []
            auto_save_lst = []
            if len(scripts) > 1:
                for s in scripts[1:]:
                    filename = s.splitlines()[0][1:]
                    file_sync_time = float(s.splitlines()[1][1:])
                    script = "\n".join(s.splitlines()[2:])
                    auto_save_lst.append((filename, script, file_sync_time))
                return auto_save_lst

        except Exception as e:
            e = type(e)(("Autosaved scripts could not be loaded from:\n'%s'\n" %
                         self.autosave_filename) + str(e)).with_traceback(sys.exc_info()[2])
            self.show_error(e)
            sys.stderr.write(traceback.format_exc())
        return [("New", "", 0)]

    def _actionNew(self, filename="New", script=None, file_sync_time=0):
        # Todo: ask if script should be saved
        e = self.new_script_editor()
        e.set_script(filename, script, file_sync_time)
        e.update_dirty(refresh=True)
        return e

    def new_script_editor(self):
        return self.scriptEditor

    def _actionOpen(self, filename=""):
        if filename == "":
            filename = self.gui.get_open_filename("Open script", "*.py")
            if filename == '':
                return
        if os.path.isfile(filename):
            self._actionNew(filename)

    def _actionSave(self):
        if os.path.isfile(self.scriptEditor.filename):
            self.scriptEditor.save()
        else:
            self._actionSaveAs()

    def _actionSaveAs(self):
        folder = os.path.dirname(self.scriptEditor.filename)
        filename = self.gui.get_save_filename("Save script", "*.py", folder, "*.py", self.__class__.__name__)
        if filename != "":
            try:
                with open(filename, 'w'):
                    pass
            except Exception:
                pass
            self.scriptEditor.name = filename
            self._actionSave()

    @property
    def findDialog(self):
        if not hasattr(self, '_findDialog'):
            self._findDialog = FindDialog(self)
            self._findDialog.find.connect(self.actionFind)
            self._findDialog.find_next.connect(self.actionFind_next)
            self._findDialog.find_previous.connect(self.actionFind_previous)
        return self._findDialog

    def _actionFind(self, text=None):
        if text is None:
            self.findDialog.show()
        else:
            self.scriptEditor.find(text, case_sensitive=self.findDialog.isCaseSensitive(),
                                   whole_word_only=self.findDialog.isWholeWord())

    def _actionFind_next(self):
        if self.findDialog.getFindText() == "":
            return self.actionFind()
        self.scriptEditor.find_next()

    def _actionFind_previous(self):
        if self.findDialog.getFindText() == "":
            return self.actionFind()
        self.scriptEditor.find_previous()

    def _actionComment(self):
        self.scriptEditor.comment_selection()

    def _actionUncomment(self):
        self.scriptEditor.uncomment_selection()

    def cleanUp(self):
        try:
            self.autosave()
        except Exception:
            sys.stderr.write(traceback.format_exc())
        QtMainWindowLoader.cleanUp(self)

    def _actionRunScript(self):
        with self.gui.wait_cursor():
            self.autosave()
            self.set_output("")
            self.ui.labelLineNumber.setText("")
            QApplication.processEvents()
            sys.stdout = self
            starttime = -time.time()
            self.output = []

            try:
                return_value = self.scriptRunner.run(self.scriptEditor.script)
                self.set_output("".join(self.output))
                beep(1000, 50)
                return return_value
            except (Warning, Exception):
                try:
                    exc_txt = traceback.format_exc()
                    exc_lines = exc_txt.split("\n")
                    if ', in Script' in exc_txt:
                        exc_lines = exc_lines[0:1] + \
                            exc_lines[[l.endswith(', in Script') for l in exc_lines].index(True):]
                    else:
                        exc_lines = exc_lines[0:1] + \
                            exc_lines[[l.startswith('  File "<string>", line') for l in exc_lines].index(True):]

                    def line2linenr(line):
                        lbl = line[line.index(", line ") + 7:]
                        if ", in" in lbl:
                            lbl = lbl[:lbl.index(", in")]
                        return int(lbl)
                    # decrement line numbers  to exclude first line: def Script():
                    for i, l in enumerate(exc_lines):
                        if l.startswith('  File "<string>"'):
                            line_nr = line2linenr(l) - 1
                            exc_lines[i] = l.replace("line %d," % (line_nr + 1), "line %d," % (line_nr))
                    self.activateWindow()
                    self.scriptEditor.setFocus()
                    self.scriptEditor.select_line(line_nr - 1)
                except Exception:
                    traceback.print_exc(file=sys.stdout)
                self.output.extend("\n".join(['', '-' * 30] + exc_lines))
                if hasattr(sys.__stderr__, 'write'):
                    sys.__stderr__.write("\n".join(exc_lines))
                beep(500, 50)
            finally:
                self.set_output("".join(self.output))
                sys.stdout = sys.__stdout__
                self.ui.labelLineNumber.setText("Script executed in %d seconds" % (time.time() + starttime))

    def flush(self):
        pass  # solves problem on linux

    def write(self, s):
        try:
            sys.__stdout__.write(s)
        except (IOError, AttributeError):
            # flush not working in no console cx_freeze application
            # __stdout__ is None and has no write method in cx_freeze application
            pass
        self.output.append(s)

    def set_output(self, output):
        self.ui.textEditOutput.setText(output)


class TabbedScriptingWindow(ScriptingWindow):

    def __init__(self, scriptEditorClass, scriptRunner, docViewerClass=None, **kwargs):
        super(ScriptingWindow, self).__init__(**kwargs)
        self.scriptEditorClass = scriptEditorClass
        self.scriptRunner = scriptRunner
        self.scriptRunner.script_self.scriptingWindow = self
        self.tabWidget = TabWidget(self.new_script_editor)
        self.tabWidget.currentChanged.connect(self.update_title)
        self.tabWidget.tabCloseRequested.disconnect()
        self.tabWidget.tabCloseRequested.connect(self.close_tab)
        self.ui.verticalLayout_2.addWidget(self.tabWidget)
        if docViewerClass:
            self.ui.webView = docViewerClass(self.ui.splitter_2)
            self.ui.splitter_2.setSizes([1000, 300])
            self.ui.webView.setHtml("<html><body><H1>Test</h1></body></html>")
        else:
            self.ui.splitter_2.setSizes([1000, 0])

        self.autosave_filename = self.load_setting(
            'autosave_filename', os.path.expanduser('~/.%s/autosave.py' % self.app_name))

        self.tabWidget.default_tab_name = "generate_run_files"
        self.tabWidget.close_tab(0)

        for script in self.load_autosave():
            self.actionNew(*script)
        self.tabWidget.close_tab(0)
        self.tabWidget.default_tab_name = "New"
        # self.scriptEditor.setFocus()

    @property
    def scriptEditor(self):
        return self.tabWidget.currentWidget()

    @property
    def scriptEditors(self):
        return self.tabWidget.widgets()

    def _actionNew(self, filename="New", script=None, file_sync_time=0):
        i = self.tabWidget.add_tab(os.path.basename(filename))
        e = self.tabWidget.widget(i)
        e.set_script(filename, script, file_sync_time)
        e.update_dirty(refresh=True)
        e.setFocus()
        return e

    def new_script_editor(self):
        e = self.scriptEditorClass(self)
        e.get_autocomplete_list = self.scriptRunner.get_autocomplete_list
        e.name = "New"
        e.title_changed.connect(self.update_title)
        return e

    def update_title(self, title="Unknown"):
        if hasattr(self, 'tabWidget') and hasattr(self.scriptEditor, 'title'):
            self.setWindowTitle(self.scriptEditor.title)
            self.tabWidget.setTabText(self.tabWidget.currentIndex(), self.scriptEditor.title)

    def close_tab(self, index):
        if self.scriptEditor.dirty:
            if self.gui.get_confirmation("Save", "Save '%s'" % self.scriptEditor.name):
                self.actionSave()
        self.tabWidget.close_tab(index)


class ScriptingMainWindow(ScriptingWindow, QtMainWindowLoader):

    def __init__(self, icon='../Pdap.ico'):
        module = ScriptingWindowUI
        try:
            self.ui = module.Ui_Form()
        except Exception:
            pass

        QtMainWindowLoader.__init__(self, module, self)
        ScriptingWindow.__init__(self, Qscintilla2ScriptEditor, ScriptRunner())
        QtUI.__init__(self, self)
        self.setWindowIcon(QIcon(icon))


class TestScriptEditor(QtMainWindowLoader):

    def __init__(self, ui_module=None, connect_actions=True):
        QtMainWindowLoader.__init__(self, ui_module=ui_module, connect_actions=connect_actions)
        self.scriptEditor = Qscintilla2ScriptEditor(self)
        self.setCentralWidget(self.scriptEditor)
        self.addAction(QAction("Run", self, shortcut=Qt.Key_F5, triggered=self.run_script))

    def run_script(self):
        exec(self.scriptEditor.script)


class TabbedScriptingMainWindow(TabbedScriptingWindow, QtMainWindowLoader):

    def __init__(self, scriptRunner=None, docViewerClass=QTextBrowser, icon='../Pdap.ico'):
        scriptRunner = scriptRunner or ScriptRunner()
        module = ScriptingWindowUI
        try:
            self.ui = module.Ui_Form()
        except Exception:
            pass
        super().__init__(ui_module=module, scriptEditorClass=Qscintilla2ScriptEditor,
                         scriptRunner=scriptRunner, docViewerClass=docViewerClass)
        self.setWindowIcon(QIcon(icon))


class TabbedScriptingDialog(TabbedScriptingWindow, QtDialogLoader):

    def __init__(self, parent, scriptRunner=None, docViewerClass=QTextBrowser, icon='../Pdap.ico'):
        scriptRunner = scriptRunner or ScriptRunner()
        module = ScriptingWindowUI
        try:
            self.ui = module.Ui_Form()
        except Exception:
            pass
        super().__init__(ui_module=module, parent=parent, scriptEditorClass=Qscintilla2ScriptEditor,
                         scriptRunner=scriptRunner, docViewerClass=docViewerClass)
        self.gui = parent
        self.setWindowIcon(QIcon(icon))


if __name__ == "__main__":
    m = TabbedScriptingMainWindow()
    # m.scriptEditor.set_script("tests/test_files/my_script.py")
    m.start()
