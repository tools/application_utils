'''
Created on 15. feb. 2018

@author: mmpe
'''
import builtins
import os
from qtpy.QtWidgets import QMessageBox
from qtpy.QtCore import Signal


class ScriptEditor(object):
    """Base class for script editors, e.g. the QScintilla2ScriptEditor"""

    title_changed = Signal(str)
    _dirty = False
    filename = ""
    _name = ""

    def __init__(self, ui):
        self.ui = ui
        self.set_script()

    def set_script(self, name="New", script=None, file_sync_time=0):
        self.file_sync_time = file_sync_time
        if os.path.isfile(name):
            self.name = name
            if script is None:
                script = self.get_saved()
                self.file_sync_time = os.path.getmtime(self.filename)
            else:
                self.file_sync_time = file_sync_time
        else:
            self.filename = ""
            self.file_sync_time = 0
            self.name = name

        self._set_script(script)

        self.clean_txt = self.get_saved()
        self.update_dirty(refresh=True)

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        if os.path.isfile(name):
            self.filename = name
        self._name = os.path.basename(name)
        self.title_changed.emit(self.title)

    def get_saved(self):
        if self.filename and os.path.isfile(self.filename):
            f = open(self.filename)
            saved = f.read()
            f.close()
            return saved.strip()
        else:
            return ""

    def save(self):
        with open(self.filename, 'w') as fid:
            fid.write(str(self._get_script()))
        self.file_sync_time = os.path.getmtime(self.filename)
        self.update_dirty(refresh=True)

    @property
    def title(self):
        return "%s%s" % (('', '*')[self._dirty], self.name)

    def equals_saved(self):
        curr_lines = self._get_script().strip().splitlines()
        saved_lines = self.get_saved().strip().splitlines()
        if len(curr_lines) != len(saved_lines):
            return False
        for cl, sl in zip(curr_lines, saved_lines):
            if cl.strip() != sl.strip():
                return False
        return True

    @property
    def dirty(self):
        if not self._dirty:
            self._dirty = self.get_saved() != self._get_script()
            self.title_changed.emit(self.title)
        return self._dirty

    def update_dirty(self, refresh=False):
        if refresh:
            self._dirty = False
        return self.dirty

    @property
    def script(self):
        return str(self._get_script()).replace("\r", "").replace(">>> ", "").replace("\t", "    ")

    def get_autocomplete_list(self, line):
        return sorted(builtins.__dict__.keys(), key=lambda s: s.lower())

    def check_file_modified(self):
        if not hasattr(self, 'in_check_file_modified') or self.in_check_file_modified is False:
            self.in_check_file_modified = True

            if self.filename and os.path.isfile(self.filename):
                if self.equals_saved():
                    self.file_sync_time = os.path.getmtime(self.filename)
                if self.file_sync_time > 0 and str(os.path.getmtime(self.filename)) > str(self.file_sync_time):
                    msg = 'The file \'%s\' has been changed on the file system. Do you want to replace the editor contents with these changes?' % self.filename
                    msg += "\n\n" + "\n".join(["Diff in line %d:\nIn script: %s\nIn file:   %s\n" % (lnr, l1, l2) for lnr, (l1, l2)
                                               in enumerate(zip(self._get_script().split("\n"), self.get_saved().split("\n"))) if l1.strip() != l2.strip()][:5])
                    if QMessageBox.information(self, 'File changed', msg, QMessageBox.Yes | QMessageBox.No) == QMessageBox.Yes:
                        self.set_script(self.filename)
                    else:
                        self.file_sync_time = 0
            self.in_check_file_modified = False

    def close(self):
        while self.dirty is True:  # While avoids data loss, in case save operation is aborted
            if self.filename == "":
                text = "Save unsaved changes?"
            else:
                text = "Save %s?" % self.filename
            print("here")
            ans = self.ui.get_confirmation("Save", text, allow_cancel=True)

            if ans is None:
                return False
            elif ans is True:
                self.save(self.filename)
            elif ans is False:
                return True
        return True

    def _set_script(self, text):
        raise NotImplementedError

    def _get_script(self):
        raise NotImplementedError

    def find_next(self, text):
        raise NotImplementedError

    def find_previous(self, text):
        raise NotImplementedError
