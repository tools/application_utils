
import traceback

from qtpy import QtPrintSupport  # required for "from PyQt5.Qsci import QsciScintilla, QsciLexerPython, QsciAPIs" to work
from PyQt5.Qsci import QsciScintilla, QsciLexerPython, QsciAPIs
from qtpy.QtCore import QEvent
from qtpy.QtCore import Qt
from qtpy.QtGui import QFont, QFontMetrics, QColor, QKeyEvent
from qtpy.QtWidgets import QAction

from app_utils.qt_gui_loader._qt_gui_loader import QtMainWindowLoader
from app_utils.ui.scripting_window.script_editor._script_editor import ScriptEditor


class Qscintilla2ScriptEditor(ScriptEditor, QsciScintilla):
    autocomplete_lst = []

    def __init__(self, parent):
        QsciScintilla.__init__(self, parent)
        self.initialize()
        ScriptEditor.__init__(self, parent)
        self.selectionChanged.connect(self.highlight_occurences)

    def _set_script(self, text):
        self.setText(text)

    def _get_script(self):
        lines = self.text().split("\n")

        # assert len(lines) < 2 or all([line == "" for line in lines[1::2]])
        # return "\n".join(lines[::2])
        return "\n".join(lines)

    def find(self, text, case_sensitive=False, whole_word_only=False):
        self.search_args = [text,
                            False,  # re
                            case_sensitive,  # cs
                            whole_word_only,   # wo
                            True]   # wrap
        self.findFirst(*self.search_args)

    def find_next(self):
        self.findFirst(*self.search_args, forward=True)

    def find_previous(self):
        self.findFirst(*self.search_args, forward=False)
        self.findNext()

    def wheelEvent(self, event):
        if event.modifiers() == Qt.ControlModifier:
            if event.angleDelta().y() > 0:
                self.zoomIn()
            else:
                self.zoomOut()
        return QsciScintilla.wheelEvent(self, event)

    def initialize(self):
        #        self.cursorPositionChanged.connect(self.self)
        #        self.copyAvailable.connect(self.self)
        #        self.indicatorClicked.connect(self.self)
        #        self.indicatorReleased.connect(self.self)
        #        self.linesChanged.connect(self.self)
        #        self.marginClicked.connect(self.self)
        #        self.modificationAttempted.connect(self.self)
        #        self.modificationChanged.connect(self.self)
        #        self.selectionChanged.connect(self.self)
        #        self.textChanged.connect(self.self)
        #        self.userListActivated.connect(self.self)

        font = QFont("Consolas", 10)
        font.setFixedPitch(True)

        self.setFont(font)
        self.setMarginsFont(font)

        # conventionnaly, margin 0 is for line numbers
        self.setMarginWidth(0, QFontMetrics(font).width("0000") + 5)
        self.setMarginLineNumbers(0, True)

        self.setTabWidth(4)

        # Folding visual : we will use boxes
        self.setFolding(QsciScintilla.BoxedTreeFoldStyle)
        self.setAutoIndent(True)

        # Braces matching
        self.setBraceMatching(QsciScintilla.SloppyBraceMatch)

        # Editing line color
        self.setCaretLineVisible(True)
        self.setCaretLineBackgroundColor(QColor("#E8E8FF"))

        # Margins colors
        # line numbers margin
        self.setMarginsBackgroundColor(QColor("#E4E4E4"))
        self.setMarginsForegroundColor(QColor("#808092"))

        # folding margin colors (foreground,background)
        self.setFoldMarginColors(QColor("#FFFFFF"), QColor("FF0000"))

        # Choose a lexer
        self.lexer = QsciLexerPython()
        self.lexer.setDefaultFont(font)

        # Set the length of the string before the editor tries to autocomplete
        # In practise this would be higher than 1
        # But its set lower here to make the autocompletion more obvious
        self.setAutoCompletionThreshold(1)
        # Tell the editor we are using a QsciAPI for the autocompletion
        self.setAutoCompletionSource(QsciScintilla.AcsAPIs)

        self.setLexer(self.lexer)
        self.setCallTipsStyle(QsciScintilla.CallTipsContext)

        # self.setCallTipsVisible(0)
        # Create an API for us to populate with our autocomplete terms
        self.api = QsciAPIs(self.lexer)

        # Compile the api for use in the lexer
        self.api.prepare()

    def keyPressEvent(self, event):
        try:

            if event.modifiers() & Qt.ControlModifier:
                if event.key() in [Qt.Key_3, Qt.Key_5]:

                    if event.key() == Qt.Key_3:
                        self.comment_selection()
                    if event.key() == Qt.Key_5:
                        self.uncomment_selection()

                    return

            QsciScintilla.keyPressEvent(self, event)

            self.update_dirty()
            linenr, pos_in_line = self.getCursorPosition()
            line = str(self.text(linenr)[:pos_in_line])

            if event.key() == Qt.Key_Enter or event.key() == Qt.Key_Return:
                for tip in self.autocomplete_lst:
                    try:
                        if line.endswith(tip[:tip.index('(')]):
                            self.insert(tip[tip.index('('):])
                            parent, residual = self.scriptRunner.split_line(line)
                            self.parent.show_documentation(
                                self.scriptRunner.get_function_dict(parent, residual)[residual])
                            return
                    except ValueError:
                        pass

            if event.key() < 100 or event.key() in [Qt.Key_Backspace, Qt.Key_Shift]:
                lst = self.get_autocomplete_list(line)

                if lst is not None:
                    self.api.clear()
                    list(map(self.api.add, lst))
                    self.api.prepare()
                    if len(lst) > 0:
                        self.autocomplete_lst = lst

                self.autoCompleteFromAll()
                shift_event = QKeyEvent(QEvent.KeyPress, Qt.Key_Shift, Qt.NoModifier)
                QsciScintilla.keyPressEvent(self, shift_event)  # show autocomplete list

        except Exception as self:
            traceback.print_exc()

    def _get_selection(self):
        l1, _, l2, _ = self.getSelection()
        if l1 == -1:
            l1 = l2 = self.getCursorPosition()[0]
        return l1, l2, self.getSelection()

    def comment_selection(self):
        l1, l2, sel = self._get_selection()
        for l in range(l1, l2 + 1):
            self.insertAt("# ", l, 0)
        self.setSelection(*sel)

    def uncomment_selection(self):
        l1, l2, sel = self._get_selection()

        for l in range(l1, l2 + 1):
            line = self.text(l)
            p = line.index("#") if "#" in line else -1
            self.setSelection(l, p, l, p + 1 + (line[p + 1] == " "))
            self.removeSelectedText()
        self.setSelection(*sel)

    def selected_word(self, select=True):
        linenr, pos_in_line = self.getCursorPosition()
        line = str(self.text(linenr))
        start, end = pos_in_line, pos_in_line
        while start > 0 and line[start - 1:end].isidentifier():
            start -= 1
        while end < len(line) and line[start:end + 1].isidentifier():
            end += 1
        if select and self.text():
            self.setSelection(linenr, start, linenr, end)
        return line[start:end]

    def select_line(self, line_nr):
        self.setSelection(line_nr, 0, line_nr, -1)
        self.ensureLineVisible(line_nr)

    def focusInEvent(self, *args, **kwargs):
        try:
            self.check_file_modified()
        except Exception:
            traceback.print_exc()
        return QsciScintilla.focusInEvent(self, *args, **kwargs)

    def highlight_occurences(self, *args, **kwargs):
        try:
            self.clearIndicatorRange(0, 0, len(self.text()), len(self.text()), 8)
            self.indicatorDefine(QsciScintilla.INDIC_STRAIGHTBOX)
            self.setIndicatorDrawUnder(True)
            self.setIndicatorForegroundColor(QColor(0, 255, 0, 50))
            self.setIndicatorOutlineColor(QColor(0, 0, 0, 50))
            w = self.selectedText()
            if w == "":
                w = self.selected_word(False)

            if w:
                for i, l in enumerate(self.text().split("\n")):
                    if w in l:
                        p = l.index(w)
                        self.fillIndicatorRange(i, p, i, p + len(w), 8)
        except Exception:
            pass

    def terminate(self):
        print("terminate script")


class TestScriptEditor(QtMainWindowLoader):

    def __init__(self, ui_module=None, connect_actions=True):
        QtMainWindowLoader.__init__(self, ui_module=ui_module, connect_actions=connect_actions)
        self.scriptEditor = Qscintilla2ScriptEditor(self)
        self.setCentralWidget(self.scriptEditor)
        self.addAction(QAction("Run", self, shortcut=Qt.Key_F5, triggered=self.run_script))

    def run_script(self):
        exec(self.scriptEditor.script)


if __name__ == "__main__":
    m = TestScriptEditor()
    m.start()
