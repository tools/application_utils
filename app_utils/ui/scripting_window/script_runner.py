import inspect
import types
from app_utils.ui.scripting_window.script_function import ScriptFunction
from app_utils.functions.inspect_ext import argument_string
import os


class ScriptRunner(object):

    def __init__(self, self_dict={}, script_globals={}):
        self.last = None

        class ScriptSelf(object):
            def __init__(self):
                self.__class__.__name__ = 'self'

        self.script_self = ScriptSelf()
        for k, v in {**self_dict, 'scriptRunner': self}.items():
            setattr(self.script_self, k, v)

        self.script_globals = self.get_globals()
        self.script_globals.update(script_globals)

    def get_globals(self):
        gl = {k: v for (k, v) in globals().items()}
        gl['self'] = self.script_self
        return gl

    def run(self, script_code):
        if os.path.isfile(script_code):
            with open(script_code) as fid:
                script_code = fid.read()

        self.script_code = script_code
        self.script_lines = script_code.replace("\t", "    ").split("\n")
        s = """def Script():
%s
return_value = Script()""" % ("\n".join(["    " + l for l in self.script_lines]))
        self.script_globals['return_value'] = None
        exec(s, self.script_globals)
        return self.script_globals['return_value']

    def split_line(self, line):
        txt = line.strip()
        for c in " ();":
            txt = txt.split(c)[-1]

        parent = ".".join(txt.split(".")[:-1])

        residual = txt.split(".")[-1]
        return parent, residual

    def get_function_dict(self, parent, residual):
        dir_dict = {}
        if parent == "":
            for d in [self.script_globals, __builtins__]:
                dir_dict.update(d)
        else:
            try:
                parent_obj = eval(parent)
                if inspect.isclass(parent_obj):
                    parent_cls = parent_obj
                else:
                    if hasattr(parent_obj, '__dict__'):
                        dir_dict.update(vars(parent_obj))
                    parent_cls = parent_obj.__class__
                for d in [vars(obj) for obj in inspect.getmro(parent_cls)]:
                    dir_dict.update(d)
            except (NameError, AttributeError, SyntaxError):
                parent_obj = None

        object_dict = {k: v for k, v in dir_dict.items() if k.lower().startswith(residual.lower())}
        func_dict = {}
        for name, obj in object_dict.items():
            if isinstance(obj, ScriptFunction):
                obj = obj.run
                if obj.__name__ == "new_f":
                    obj = obj.f  # model changer decorator
            if inspect.isclass(obj):
                obj = obj.__init__

            if isinstance(obj, (types.FunctionType, types.MethodType, types.BuiltinFunctionType)):
                func_dict[name] = obj
        return func_dict

    def get_autocomplete_list_old(self, line):
        parent, residual = self.split_line(line)
        if parent == self.last and self.last != '':
            return None
        else:
            self.last = parent
            return [(syntax + argument_string(func))
                    for syntax, func in self.get_function_dict(parent, residual).items()]

    def get_autocomplete_list(self, line):
        parent, name = self.split_line(line)
        lst = []
        if parent == self.last:
            return None
        else:
            print(parent, name)

            def add2lst(obj_dict):
                if len(name) == 0:
                    lst.extend([k for k in obj_dict.keys() if k[0] != "_"])
                elif name == "_":
                    lst.extend([k for k in obj_dict.keys() if k[0] == "_" and k[:2] != "__"])
                else:
                    lst.extend([k for k in obj_dict.keys() if k.startswith(name)])
            if parent == "":
                add2lst(self.script_globals)
                add2lst(__builtins__)
            elif parent:
                try:
                    parent_obj = eval(parent)
                    add2lst(vars(parent_obj))
                except (NameError, AttributeError, SyntaxError):
                    parent_obj = None
            self.last = parent
            return sorted(lst, key=lambda s: s.lower())


class AppFuncScriptRunner(ScriptRunner):

    def __init__(self, controller, gui, model):
        self.controller = controller
        self.model = model
        self.gui = gui
        self.last = None

        class ScriptSelf(object):

            def __init__(self, controller, gui, model):
                self.controller = controller
                self.gui = gui
                self.model = model
        self.script_self = ScriptSelf(controller, gui, model)
        self.script_self.scriptRunner = self
        self.script_globals = self.get_globals()
        self.script_globals['controller'] = controller
        self.script_globals['model'] = model
        self.script_globals['gui'] = gui

        if hasattr(self.controller, 'appFuncs'):
            for func in self.controller.appFuncs:
                setattr(self, func.class_name, func)
                self.script_globals[func.class_name] = func
