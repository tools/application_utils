def get_version_tuple(version_str):
    def int_or_str(s):
        try:
            return int(s)
        except BaseException:
            return s
    version = tuple(map(int_or_str, version_str.split(".")))
    assert all([isinstance(v, int) for v in version[:2]]), version
    return version
