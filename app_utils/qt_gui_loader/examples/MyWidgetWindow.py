"""Use as ui_widget with actionhandlers at parent(e.g. QMainWindow) subclass """

import sys

from qtpy.QtWidgets import QMainWindow, QApplication

from app_utils.qt_gui_loader._qt_gui_loader import QtWidgetLoader
from app_utils.qt_gui_loader.examples.ui import MyWidgetUI
from PyQt5.Qt import QTimer


class WidgetWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        ui_module = MyWidgetUI
        try:
            self.ui = ui_module.Ui_Form()  # enable autocomplete
        except Exception:
            pass
        super().__init__()
        self.widget = QtWidgetLoader(ui_module=ui_module, parent=self, action_receiver=self)
        self.widget.ui.lineEdit.setText("MyWidget")
        self.show()

    def actionPrintText(self):
        print("Widget text: %s" % self.widget.ui.lineEdit.text())


def run_example():
    if __name__ == '__main__':
        app = QApplication(sys.argv)
        WidgetWindow()
        if 'terminate' in sys.argv:
            QTimer().singleShot(100, QApplication.quit)
        app.exec_()


run_example()
