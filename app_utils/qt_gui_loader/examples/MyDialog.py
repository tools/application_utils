"""Use as Dialog (i.e. sub window, that closes with parent)"""
from app_utils.qt_gui_loader._qt_gui_loader import QtDialogLoader
from app_utils.qt_gui_loader.examples.ui import MyWidgetUI


class MyDialog(QtDialogLoader):
    def __init__(self, parent, modal):
        ui_module = MyWidgetUI
        try:
            self.ui = ui_module.Ui_Form()  # enable autocomplete
        except Exception:
            pass
        QtDialogLoader.__init__(self, ui_module, parent, modal)
        self.ui.lineEdit.setText("MyDialog")
        self.setWindowTitle("MyDialog")

    def actionPrintText(self):
        print("Dialog text: %s" % self.ui.lineEdit.text())


# run using
def run_example():
    if __name__ == '__main__':
        MyDialog(None, True).start()


run_example()
