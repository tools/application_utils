from pprint import pprint

from PyQt5.QtWidgets import QLineEdit, QPushButton, QHBoxLayout, QWidget

from app_utils.qt_gui_loader._qt_gui_loader import QtMainWindowLoader


class MyEmptyMainWindow(QtMainWindowLoader):
    def __init__(self, parent=None):
        super().__init__()
        self.setCentralWidget(QWidget(self))
        hbox = QHBoxLayout()
        self.lineEdit = QLineEdit("MyMainWindow", self)
        self.pushButton = QPushButton("Ok", self, clicked=self.actionPrintText)

        hbox.addWidget(self.lineEdit)
        hbox.addWidget(self.pushButton)
        self.centralWidget().setLayout(hbox)
        self.ui.lineEdit.setText("MyMainWindow")
        self.setWindowTitle("MyMainWindow")

    def actionPrintText(self, *args):
        print("Mainwindow text: %s" % self.ui.lineEdit.text())


def run_example():
    if __name__ == '__main__':
        MyEmptyMainWindow().start()


run_example()
