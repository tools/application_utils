"""Use as Dialog (i.e. sub window, that closes with parent)"""
from app_utils.qt_gui_loader._qt_gui_loader import QtDialogLoader, QtMainWindowLoader
from app_utils.qt_gui_loader.examples.ui import MyWidgetUI, MyMainWindowUI
from app_utils.ui.scripting_window._scripting_window import TabbedScriptingDialog


class MyDialog(QtDialogLoader):
    def __init__(self, parent, modal):
        ui_module = MyWidgetUI
        try:
            self.ui = ui_module.Ui_Form()  # enable autocomplete
        except Exception:
            pass
        QtDialogLoader.__init__(self, ui_module, parent, modal)
        self.ui.lineEdit.setText("MyDialog")
        self.setWindowTitle("MyDialog")

    def actionPrintText(self):
        print("Dialog text: %s" % self.ui.lineEdit.text())


class MyMainWindow(QtMainWindowLoader):
    def __init__(self, parent=None):
        ui_module = MyMainWindowUI
        try:
            self.ui = ui_module.Ui_Form()  # enable autocomplete
        except Exception:
            pass
        super().__init__(ui_module)

    def actionOpenDialog(self):
        TabbedScriptingDialog(self).start()


def run_example():
    if __name__ == '__main__':
        MyMainWindow().start()


run_example()
