from app_utils.qt_gui_loader._qt_gui_loader import QtMainWindowLoader
from app_utils.qt_gui_loader.examples.ui import MyWidgetUI


class MyMainWindow(QtMainWindowLoader):
    def __init__(self, parent=None):
        ui_module = MyWidgetUI
        try:
            self.ui = ui_module.Ui_Form()  # enable autocomplete
        except Exception:
            pass
        super().__init__(ui_module)

        self.ui.lineEdit.setText("MyMainWindow")
        self.setWindowTitle("MyMainWindow")

    def actionPrintText(self, *args):
        print("Mainwindow text: %s" % self.ui.lineEdit.text())


def run_example():
    if __name__ == '__main__':
        MyMainWindow().start()


run_example()
