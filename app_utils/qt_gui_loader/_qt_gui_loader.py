'''
Classes for loading a QWidget designed in QT Designer as MainWindow, Dialog or Widget
Examples of how to use can be found in UseQtGuiLoader.py
'''


import inspect
import os
import sys

import qtpy
from qtpy.QtCore import QSettings, QTimer, QEvent
from qtpy.QtWidgets import QAction, QGridLayout, QApplication, QWidget, QDialog
from qtpy.QtWidgets import QMainWindow
import importlib
from functools import wraps
import traceback
from app_utils.ui._ui import OutputUI
from app_utils.ui.qt_ui import QtUI
from app_utils.ui.qt_progress_information import CancelWarning
from PyQt5 import QtCore, QtWidgets


def pyqt_compile_func(ui_file, py_file):
    # pyuic_path = os.path.join(os.path.dirname(sys.executable), 'Lib/site-packages/%s/uic/pyuic.py'%qtpy.API)
    os.system('"%s" -m %s.uic.pyuic -x %s -o %s' %
              (sys.executable, qtpy.QtCore.Qt.__module__.replace(".QtCore", ""), ui_file, py_file))


def excepthook(exc_type, exc_value, exc_tb):
    e = exc_type(exc_value).with_traceback(exc_tb)
    sys.__stdout__.write(f'{"".join(traceback.format_exception(e))}\n')
    QtUI().show_error(e, show_traceback=True)


class QtGuiLoader():

    def __init__(self, **_):
        super().__init__()

    def compile_ui(self, ui_module, recompile=False):
        basename = os.path.splitext(os.path.abspath(ui_module.__file__))[0]
        ui_file = basename + ".ui"
        py_file = basename + ".py"

        if os.path.exists(ui_file):
            if not os.path.exists(py_file) or \
                    os.path.getmtime(ui_file) > os.path.getmtime(py_file) or \
                    os.path.getsize(py_file) == 0 or \
                    recompile:
                print("compile %s > %s" % (ui_file, py_file))
                pyqt_compile_func(ui_file, py_file)
        importlib.reload(ui_module)

    def connect_actions(self, action_receiver=None):
        if not hasattr(self, 'run') and hasattr(self.parent(), 'run'):
            self.run = self.parent().run
        for name, action in [(n, a) for n, a in vars(self.ui).items() if isinstance(a, QAction)]:
            if action_receiver is None:
                action_receiver = self
            if hasattr(action_receiver, "_" + name) and hasattr(self, "run") and hasattr(self, 'gui'):
                func = getattr(action_receiver, "_" + name)

                def action_wrapper(f):
                    def wrapper(*args, **kwargs):
                        if args == (False,) and kwargs == {}:
                            # Remove default pyqt5 (and qtpy) check=False argument
                            return self.gui.run(f)
                        else:
                            return self.gui.run(f, *args, **kwargs)
                    return wrapper

                setattr(action_receiver, name, action_wrapper(func))

            if hasattr(action_receiver, name):
                action.triggered.connect(getattr(action_receiver, name))
            elif not hasattr(action_receiver, "_" + name):
                try:
                    source_file = inspect.getsourcefile(self.__class__)
                    class_source = inspect.getsource(self.__class__)
                    func_source = """
    def _%s(self):
        #Auto implemented action handler
        raise NotImplementedError()
""" % name

                    with open(source_file, 'r+') as fid:
                        source = fid.read().replace(class_source, class_source + func_source)
                        fid.seek(0)
                        fid.write(source)
                    print("Missing method '_%s' appended to class %s" % (name, self.__class__.__name__))
                except Exception:
                    raise Warning("Action %s not connected. Method with name '%s' not found and autogeneration failed" % (
                        action.text(), name))

    def setupUI(self, widget):
        self.ui.setupUi(widget)
        root_widgets = [w for w in widget.children() if w.__class__.__name__ == "QWidget"]
        if len(root_widgets) == 0 or widget.layout() is not None:
            self.ui_widget = self
        else:
            self.ui_widget = root_widgets[-1]
            g = QGridLayout()
            if qtpy.API == "pyqt5":
                g.setContentsMargins(0, 0, 0, 0)
            else:
                if isinstance(self, QtWidgetLoader):
                    g.setMargin(0)
                    g.setSpacing(0)
            widget.setLayout(g)

            g.addWidget(self.ui_widget)


class QtGuiApplication(object):

    def __init__(self, ui_module, **kwargs):

        self.ui_module = ui_module
        self.app_filename = os.path.basename(sys.argv[0])
        self.app_name = os.path.splitext(self.app_filename)[0]
        if os.name == 'posix' and 'DISPLAY' not in os.environ:
            os.environ['QT_QPA_PLATFORM'] = "offscreen"
        #     raise Exception('No display available')
        if QApplication.startingUp():
            self.app = QApplication(sys.argv)
        else:
            self.app = QApplication.instance()
        super().__init__(**kwargs)
        if self.ui_module is not None:
            if not hasattr(self.ui_module, '__name__'):
                self.ui_module.__name__ = self.ui_module.__class__.__name__
            if hasattr(self, 'compile_ui'):
                self.compile_ui(ui_module)

    def save_settings(self):
        settings = QSettings("QtGuiApplication", "%s_%s" % (self.app_name, self.__class__.__name__))
        settings.setValue(self.ui_module.__name__ + "/geometry", self.saveGeometry())
        try:
            settings.setValue(self.ui_module.__name__ + "/windowState", self.saveState())
            settings.setValue(self.ui_module.__name__ + "/maximized", self.windowState() & QtCore.Qt.WindowMaximized)
        except Exception:
            pass

    def load_settings(self):
        settings = QSettings("QtGuiApplication", "%s_%s" % (self.app_name, self.__class__.__name__))
        try:
            self.restoreGeometry(settings.value(self.ui_module.__name__ + "/geometry"))
            self.restoreState(settings.value(self.ui_module.__name__ + "/windowState"))
            if settings.value(self.ui_module.__name__ + "/maximized", True):
                self.showMaximized()
        except Exception as e:
            pass  # Fails in PySide

    def save_setting(self, key, value):
        settings = QSettings("QtGuiApplication", "%s_%s" % (self.app_name, self.__class__.__name__))
        settings.setValue(self.ui_module.__name__ + "/" + key, value)

    def load_setting(self, key, default_value=None):
        settings = QSettings("QtGuiApplication", "%s_%s" % (self.app_name, self.__class__.__name__))
        setting = settings.value(self.ui_module.__name__ + "/" + key, default_value)
        try:
            setting = setting.toString()
        except Exception:
            pass  # fails in pyside
        return str(setting)

    def clear_settings(self):
        settings = QSettings("QtGuiApplication", "%s_%s" % (self.app_name, self.__class__.__name__))
        settings.clear()


class QtMainWindowLoader(QtGuiApplication, QtGuiLoader, QMainWindow, QtUI):
    """Load QtGui as MainWindow

    Examples
    --------

    class MyMainWindow(QtMainWindowLoader):
        def __init__(self, parent=None):
            ui_module = MyWidgetUI
            try: self.ui = ui_module.Ui_Form()  #enable autocomplete
            except: pass
            QtMainWindowLoader.__init__(self, ui_module)
            self.ui.lineEdit.setText("MyMainWindow")
            self.setWindowTitle("MyMainWindow")

        def actionPrintText(self):
            print ("Mainwindow text: %s" % self.ui.lineEdit.text())
    """

    def __init__(self, ui_module=None, connect_actions=True, **kwargs):
        self.gui = self
        super().__init__(ui_module=ui_module, **kwargs)
        # QtGuiApplication.__init__(self, ui_module)
        # QtWidgets.QMainWindow.__init__(self)
        # super(QtWidgets.QMainWindow, self).__init__(self)
        # super(QtMainWindowLoader, self).__init__(self)

        if ui_module is None:
            self.ui_module = self
            self.ui = self
            self.__name__ = self.__class__.__name__

        if "Ui_Form" in dir(ui_module):
            centralWidget = QWidget(self)
            self.ui = ui_module.Ui_Form()
            self.setCentralWidget(centralWidget)
            try:
                self.setupUI(centralWidget)
            except TypeError:
                self.compile_ui(ui_module, True)
                self.ui = ui_module.Ui_Form()
                self.setupUI(centralWidget)
#
        elif "Ui_MainWindow" in dir(ui_module):
            self.ui = ui_module.Ui_MainWindow()

            try:
                self.ui.setupUi(self)
            except TypeError:
                self.compile_ui(ui_module, True)
                self.ui = ui_module.Ui_MainWindow()
                self.ui.setupUi(self)

        if connect_actions:
            self.connect_actions()

    def start(self):
        try:

            self.show()
            self.load_settings()

            if hasattr(self, "app"):
                QTimer().singleShot(100, self._post_initialize)
                self.app.aboutToQuit.connect(self.cleanUp)

                sys.excepthook = excepthook
                QApplication.processEvents()
                res = self.app.exec()
                print("Exit", res)
                sys.exit(res)
        except Exception as e:
            sys.stderr.write(str(e))

    def _post_initialize(self):
        try:
            self.post_initialize()
        except Exception as e:
            if sys.stderr:
                sys.stderr.write(str(e))

    def post_initialize(self):
        if sys.argv[-1] == "terminate":
            self.terminate()

    def cleanUp(self):
        return

        def clean(item):
            """Clean up the memory by closing and deleting the item if possible."""
            if isinstance(item, list):
                while (len(item)):
                    clean(item.pop())
            elif isinstance(item, dict):
                while (len(item)):
                    clean(item.popitem())
            if isinstance(item, type):
                pass
            else:
                try:
                    item.close()
                except (RuntimeError, AttributeError):  # deleted or no close method
                    pass
                try:
                    item.deleteLater()
                except (RuntimeError, AttributeError):  # deleted or no deleteLater method
                    pass

        for i in list(self.__dict__.keys()):
            if i in self.__dict__:
                item = self.__dict__[i]
                clean(item)

    def terminate(self):
        QApplication.quit()

    def closeEvent(self, *args, **kwargs):
        self.save_settings()
        # Enable paste of clipboard after termination
        clipboard = QApplication.clipboard()
        event = QEvent(QEvent.Clipboard)
        QApplication.sendEvent(clipboard, event)
        return QMainWindow.closeEvent(self, *args, **kwargs)


class QtDialogLoader(QtGuiLoader, QtGuiApplication, QDialog):

    def __init__(self, ui_module, parent, modal=True, connect_actions=True):
        self.gui = parent
        QtGuiApplication.__init__(self, ui_module)
        QDialog.__init__(self, parent)
        self.modal = modal
        self.setModal(modal)
        try:
            self.ui = ui_module.Ui_Form()
            self.setupUI(self)
        except Exception:
            self.compile_ui(ui_module, True)
            self.ui = ui_module.Ui_Form()
            self.setupUI(self)

        if connect_actions:
            self.connect_actions()

    def start(self):
        self.load_settings()
        self.show()
        self.raise_()
        sys.excepthook = excepthook
        if hasattr(self, "app"):
            return self.app.exec()
        elif self.modal:
            return self.exec_()

    def hideEvent(self, *args, **kwargs):
        self.save_settings()
        if isinstance(self, QDialog):
            return QDialog.hideEvent(self, *args, **kwargs)


class QtWidgetLoader(QtGuiLoader, QWidget):

    def __init__(self, ui_module=None, action_receiver=None, parent=None, connect_actions=True):
        if "ui_module" not in vars(self):
            QWidget.__init__(self, parent)
            self.gui = parent
            if ui_module:
                self.ui_module = ui_module
                self.compile_ui(ui_module)
                self.ui = ui_module.Ui_Form()
                try:
                    self.setupUI(self)
                except Exception:
                    self.compile_ui(ui_module, True)
                    self.ui = ui_module.Ui_Form()
                    self.setupUI(self)

                if connect_actions:
                    self.connect_actions(action_receiver)

    def _actionApply(self):
        # Auto implemented action handler
        raise NotImplementedError


def run_gui_application(get_mainwindow_class, args=sys.argv[1:]):
    try:
        import os
        import sys
        from app_utils.ui.qt_ui import QtUI
        qtui = QtUI(None)
        sys.stdout = qtui.stdout
        sys.stderr = qtui.stderr

        MW = get_mainwindow_class()

    except Exception as e:
        from app_utils.ui.qt_ui import QtUI
        QtUI(None).show_error(e, show_traceback=True)
        raise
    finally:
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__

    try:
        mw = MW(args)
        mw.start()
        return mw
    except Exception as e:
        if hasattr(sys, 'frozen'):
            from app_utils.ui.qt_ui import QtUI
            QtUI(None).show_error(e, show_traceback=True)
        raise
