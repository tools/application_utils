import os
import sys
import sysconfig
import shutil
import PyInstaller


def build_pyinstaller_exe(filename, version, platform, onefile=True, build_args=[]):
    args = []
    if 'console' in build_args:
        build_args.remove('console')
    else:
        # args.append("--noconsole")
        pass

    if onefile:
        args.append("--onefile")
    args.append('--noupx')
    args.append('--noconfirm')
    for arg in build_args:
        args.append(arg)
    # args.append('--log-level=DEBUG')
    if hasattr(filename, "__file__"):
        filename = filename.__file__
    from PyInstaller import __main__
    shutil.rmtree("./dist/", ignore_errors=True,)
    __main__.run([filename] + args)

    print("Build done, move to dist")
    filename = os.path.basename(filename)
    try:
        if onefile:
            os.rename("./dist/%s" % filename.replace(".py", ".exe"), "./dist/%s" %
                      filename.replace(".py", "-%s-%s.exe" % (version, platform)))
        else:
            os.rename("./dist/%s/" % filename.replace(".py", ""),
                      "./dist/%s" % filename.replace(".py", "-%s-%s/" % (version, platform)))
    except Exception:
        raise


def make_spec(pyinstaller_path, filename, window=True, onefile=True, build_args=[]):
    args = []
    if window:
        args.append("--noconsole")
    if onefile:
        args.append("--onefile")
    args.append('--noupx')
    for arg in build_args:
        args.append(arg)
    # args.append('--log-level=DEBUG')
    if hasattr(filename, "__file__"):
        filename = filename.__file__
    cmd = '%s %s %s' % (os.path.join(pyinstaller_path, 'pyi-makespec.exe'), " ".join(args), filename)
    print(cmd)
    shutil.rmtree("./dist/", ignore_errors=True,)
    os.system(cmd)


def build_from_spec(filename, spec_name, version="1.0.0"):
    if hasattr(filename, "__file__"):
        filename = filename.__file__
    from PyInstaller import __main__
    shutil.rmtree("./dist/", ignore_errors=True,)
    __main__.run(spec_name)

    filename = os.path.basename(filename)
    os.rename("./dist/%s/" % filename.replace(".py", ""), "./dist/%s" %
              filename.replace(".py", "-%s-%s/" % (version, sysconfig.get_platform())))
