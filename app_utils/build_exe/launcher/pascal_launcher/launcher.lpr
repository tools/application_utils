program launcher;
  uses sysutils, Classes, StrUtils;

Var Info : TSearchRec;
    name, ext, args : string;
    version_lst: TStringList;
    i,j : integer;


function VersionCompare(List: TStringList; Index1, Index: Integer): Integer;
var
    v1,v : string;
begin
     v1:=ExtractDelimited(2,List[index1],['-']);
     v:=ExtractDelimited(2,List[index],['-']);
     for i:=1 to WordCount(v1,['.'])+1 do
     begin
       if StrToInt(ExtractDelimited(i,v1,['.']))<StrToInt(ExtractDelimited(i,v,['.'])) then
       begin
          result:=1;
          break;
       end
       else if StrToInt(ExtractDelimited(i,v1,['.']))>StrToInt(ExtractDelimited(i,v,['.'])) then
       begin
          result:=-1;
          break;
       end
       else
          result:=1;
     end

end;

begin

    name:=  ExtractFileName(ParamStr(0))   ;
  ext:= extractFileExt(name);

  name:=StringReplace(name, ext,'', [rfReplaceAll, rfIgnoreCase]);

        version_lst := TStringList.Create; // This is needed when using this class(or most classes)
    If FindFirst (name+'-*',faDirectory,Info)=0 then
      begin
      Repeat
        With Info do
          begin
//            writeln(name);
            version_lst.Add(name);
          end;
      Until FindNext(info)<>0;
      end;
    FindClose(Info);
    version_lst.CustomSort(@VersionCompare);

    for i:=0 to version_lst.count-1 do
    begin
        try
//           writeln(version_lst[i]);
//           writeln(version_lst[i] + '/' + name + '.exe');

             For j:=1 to ParamCount do
                 args := args + ParamStr (j) + ' ';
           SysUtils.ExecuteProcess(version_lst[i] + '/' + name, args);
           break;
        finally
        end;
    end;

    version_lst.Free; //Release the memory used by this stringlist instance


end.

