import requests
import json


import machineid
import platform


def register_usage(version, ACCOUNT, LICENSE_ID, TOKEN):
    FINGERPRINT = machineid.id()
    PLATFORM = platform.system()

    # Attempt to create new machine
    res = requests.post(
        f"https://api.keygen.sh/v1/accounts/{ACCOUNT}/machines",
        headers={
            "Content-Type": "application/vnd.api+json",
            "Accept": "application/vnd.api+json",
            "Authorization": f"Bearer {TOKEN}"
        },
        data=json.dumps({
            "data": {
                "type": "machines",
                "attributes": {
                    "fingerprint": f"{FINGERPRINT}",
                    "platform": f"{PLATFORM}",
                    "name": f"{version}",
                },
                "relationships": {
                    "license": {
                        "data": {
                            "type": "licenses",
                            "id": f"{LICENSE_ID}"
                        }
                    }
                }
            }
        })
    ).json()

    # Update machine if it already exists
    if ("errors" in res.keys() and res["errors"][0]["code"].lower() == "fingerprint_taken"):
        res = requests.patch(
            f"https://api.keygen.sh/v1/accounts/{ACCOUNT}/machines/{FINGERPRINT}",
            headers={
                "Content-Type": "application/vnd.api+json",
                "Accept": "application/vnd.api+json",
                "Authorization": f"Bearer {TOKEN}"
            },
            data=json.dumps({
                "data": {
                    "type": "machines",
                    "attributes": {
                        "name": f"{version}"
                    }
                }
            })
        ).json()

    # Increment use
    res = requests.post(
        f"https://api.keygen.sh/v1/accounts/{ACCOUNT}/licenses/{LICENSE_ID}/actions/increment-usage",
        headers={
            "Content-Type": "application/vnd.api+json",
            "Accept": "application/vnd.api+json",
            "Authorization": f"Bearer {TOKEN}"
        },
        data=json.dumps({
            "meta": {
                "increment": 1
            }
        })
    ).json()


def get_usage(LICENSE_ID, TOKEN):

    # General
    ACCOUNT = "0244d026-758b-442b-9668-811ce125a7e3"

    res = requests.get(
        f"https://api.keygen.sh/v1/accounts/{ACCOUNT}/licenses/{LICENSE_ID}",
        headers={
            "Accept": "application/vnd.api+json",
            f"Authorization": f"Bearer {TOKEN}"
        }
    ).json()

    # Get total number of uses
    n_uses = res["data"]["attributes"]["uses"]

    # Get total number of users
    n_machines = res["data"]["relationships"]["machines"]["meta"]["count"]

    # Get a list of all version numbers (machine names)
    res = requests.get(f"https://api.keygen.sh/v1/accounts/{ACCOUNT}/machines?license={LICENSE_ID}",
                       headers={"Accept": "application/vnd.api+json",
                                f"Authorization": f"Bearer {TOKEN}"}).json()

    # Get list of versions used
    versions = [r["attributes"]["name"] for r in res['data']]

    platforms = [r["attributes"]["platform"] for r in res['data']]
    return n_uses, n_machines, versions, platforms
