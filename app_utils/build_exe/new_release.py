from app_utils.utils import git_utils
from pathlib import Path
from app_utils.build_exe.make_standard_installer import make_standard_installer
from app_utils.utils.git_utils import get_git_version


def next_tag(tag, release_type=2):
    return ".".join([str([int(n), 0][i > release_type] + (0, 1)[i == release_type])
                     for i, n in enumerate(tag.split("."))])


def make_new_release(module, app, build_args=[], exclude=[],
                     PRETEST=1,
                     BUILD_EXE=1,
                     TEST=1,
                     MAKE_ZIP=1,
                     UPLOAD=1,
                     DELETE_UNPACKED=0,
                     COPY_SOURCE=0
                     ):

    current_version = git_utils.set_version_from_git(module)
    if current_version.split(".")[-1] != '0':
        release_type = 2
    elif current_version.split(".")[-2] != '0':
        release_type = 1
    else:
        release_type = 0

    build_args.extend([f'--exclude-module={ex}' for ex in exclude])

    make_standard_installer(app,
                            build_args=build_args,  # ("--hidden-import=scipy._lib.messagestream",),
                            PRETEST=PRETEST,
                            BUILD_EXE=BUILD_EXE,
                            TEST=TEST,
                            MAKE_ZIP=MAKE_ZIP,
                            UPLOAD=UPLOAD,
                            DELETE_UNPACKED=DELETE_UNPACKED,
                            COPY_SOURCE=COPY_SOURCE)

    print('Done releasing', current_version)


def update_tag(module, release_type=2):
    current_version = get_git_version(module.__file__)
    if 'dirty' in current_version:
        raise Exception("Commit before making new release")
    current_tag = git_utils.get_tag()
    if git_utils.get_git_version(module.__file__) != current_tag:
        new_tag = next_tag(current_tag, release_type)
        print("Making tag:", new_tag)
        git_utils.set_tag(new_tag, push=True)
    else:
        new_tag = current_tag
    print('Done', new_tag)


if __name__ == '__main__':
    # update_tag()
    make_new_release()
