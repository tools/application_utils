'''
Created on 8. feb. 2017

@author: mmpe
'''
import os
import shutil
import unittest

from app_utils.build_exe.auto_updater import AutoUpdater
import tempfile
import pytest
from app_utils.build_exe.make_standard_installer import get_exe_platform
platform = get_exe_platform()


@pytest.fixture
def autoUpdater():
    tmp_dir = tempfile.TemporaryDirectory()

    def get_confirmation(title, msg):
        print(title)
        print(msg)
        return True
    autoUpdater = AutoUpdater(tmp_dir.name, "Test")
    autoUpdater.ui.get_confirmation = get_confirmation
    yield autoUpdater
    tmp_dir.cleanup()


def test_Autoupdater(autoUpdater):
    for v in ["9.8.7", '999.99.99']:
        assert v in autoUpdater.versions['available']
    file_info = autoUpdater.file_dict[v][platform][f'Test-{v}-{platform}.zip']
    assert file_info == {
        'link': f'/Test/downloads/{v}/{platform}/Test-{v}-{platform}.zip',
        'size': '10.79 MB',
        'type': 'zip archive'}


def test_download_zip(autoUpdater):
    autoUpdater._download_zip('9.8.7')
    assert os.path.isfile(autoUpdater.folder + f"/Test-9.8.7-{platform}.zip")


def test_update(autoUpdater):
    os.mkdir(autoUpdater.folder + f"/Test-1.0.0-{platform}")
    os.mkdir(autoUpdater.folder + f"/Test-2.0.0-{platform}")
    assert os.path.isdir(autoUpdater.folder + f"/Test-1.0.0-{platform}")
    assert os.path.isdir(autoUpdater.folder + f"/Test-2.0.0-{platform}")
    autoUpdater.check_for_updates(1)
    assert not os.path.isdir(autoUpdater.folder + f"/Test-1.0.0-{platform}")
    assert os.path.isdir(autoUpdater.folder + f"/Test-2.0.0-{platform}")


def test_update_level1(autoUpdater):
    os.mkdir(autoUpdater.folder + f"/Test-998.98.98-{platform}")
    os.mkdir(autoUpdater.folder + f"/Test-999.98.98-{platform}")
    autoUpdater.check_for_updates(1)
    assert not os.path.isdir(autoUpdater.folder + f"/Test-999.99.99-{platform}")
    os.removedirs(autoUpdater.folder + f"/Test-999.98.98-{platform}")
    autoUpdater.check_for_updates(1)
    assert os.path.isdir(autoUpdater.folder + f"/Test-999.99.99-{platform}")


def test_update_level2(autoUpdater):
    os.mkdir(autoUpdater.folder + f"/Test-999.98.98-{platform}")
    os.mkdir(autoUpdater.folder + f"/Test-999.99.98-{platform}")
    autoUpdater.check_for_updates(2)
    assert not os.path.isdir(autoUpdater.folder + f"/Test-999.99.99-{platform}")
    os.removedirs(autoUpdater.folder + f"/Test-999.99.98-{platform}")
    autoUpdater.check_for_updates(2)
    assert os.path.isdir(autoUpdater.folder + f"/Test-999.99.99-{platform}")
