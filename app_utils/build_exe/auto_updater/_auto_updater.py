import os
from app_utils.ui.text_ui import TextUI
from app_utils.io.html import HTTP
import shutil
from tempfile import TemporaryDirectory
from urllib.error import URLError
from app_utils.utils.version_utils import get_version_tuple
from app_utils.build_exe.make_standard_installer import get_exe_platform
import json
import stat


class AutoUpdater(HTTP):

    def __init__(self, folder, app_name, keep=2, ui=TextUI()):
        HTTP.__init__(self, ui)
        self.app_name = app_name
        self.folder = self._get_app_folder(folder)
        self.keep = keep
        self._versions = None
        self._file_dict = None
        self.ui = ui
        self.url = 'https://tools.windenergy.dtu.dk'

    @property
    def versions(self):
        if self._versions is None:
            try:
                versions_dict = json.loads(self.read(self.url + '/version_inventory.json'))
                self._versions = versions_dict
            except URLError:
                self.ui.show_message(
                    f"The autoupdater could not access the update site\nYou can check manually at {self.url}")
        return self._versions[self.app_name]

    @property
    def file_dict(self):
        if self._file_dict is None:
            self._file_dict = json.loads(self.read(self.url + '/product_inventory.json'))
        return self._file_dict[self.app_name]

    def update(self, version):
        try:
            filename = self._download_zip(version)
        except Exception as e:
            url = f'{self.url}/{self.app_name}'
            e.args = ("Downloading new version failed\nYou can download the newest version manually from\n%s" % url,)
            self.ui.show_error(e)
            return
        with TemporaryDirectory() as tmp_unzip:
            import zipfile

            def unzip():
                with zipfile.ZipFile(filename, 'r') as zip_ref:
                    zip_ref.extractall(tmp_unzip)

            self.ui.exec_long_task("Unzipping downloaded version", "", False, unzip)
            os.remove(filename)
            unzip_app_folder = os.path.join(tmp_unzip, self.app_name + "/")
            for f in os.listdir(unzip_app_folder):
                try:
                    if os.path.exists(os.path.join(self.folder, f)):
                        if os.path.isfile(os.path.join(self.folder, f)):
                            os.remove(os.path.join(self.folder, f))
                        elif os.path.isdir(os.path.join(self.folder, f)):
                            shutil.rmtree(os.path.join(self.folder, f))
                    shutil.move(os.path.join(unzip_app_folder, f), self.folder)

                except (PermissionError, shutil.Error):
                    if not f.endswith(self.app_name + ".exe"):
                        raise
            if (os.name == 'posix'):
                for f in [self.app_name, f'{self.app_name}-{version}-linux64/{self.app_name}']:
                    if os.path.isfile(os.path.join(self.folder, f)):

                        st = os.stat(os.path.join(self.folder, f))
                        os.chmod(os.path.join(self.folder, f), st.st_mode | 0o111)
        self.remove_old_versions()
        self.ui.show_message("Update complete\n\nPlease restart %s" % self.app_name, "Update complete")

    def remove_old_versions(self):
        versions, version_dict = self.get_local_versions()
        if len(version_dict) > self.keep:
            for v in sorted(versions)[:-self.keep]:
                shutil.rmtree(os.path.join(self.folder, version_dict[v]))

    def _download_zip(self, version):

        zip_name = list(self.file_dict[version][get_exe_platform()].values())[0]['link']
        source = self.url + zip_name
        return self.download(source, self.folder)

    def _get_app_folder(self, folder):
        folder = os.path.abspath(folder) + "/"
        if not os.path.split(os.path.abspath(folder))[1].startswith(self.app_name):
            folder = os.path.join(folder, self.app_name)
        elif os.path.split(os.path.abspath(os.path.join(folder, "../")))[1] == self.app_name:
            folder = os.path.abspath(os.path.join(folder, "../"))
        if not os.path.isdir(folder):
            os.makedirs(folder)
        return folder

    def extract_version(self, filename):
        try:
            return get_version_tuple(os.path.basename(filename.replace(f'.{get_exe_platform()}', '')).split("-")[1])
        except Exception:
            raise Exception("Version cannot be extracted from '%s'" % filename)

    def get_local_versions(self):
        versions, version_dict = [], {}
        for f in os.listdir(self.folder):
            if os.path.isdir(os.path.join(self.folder, f)):
                try:
                    v = self.extract_version(f)
                    versions.append(v)
                    version_dict[v] = f
                except BaseException as e:
                    print(e)
        return versions, version_dict

    def check_for_updates(self, level):
        # if not hasattr(sys, "frozen"):
        #    return
        local_versions = self.get_local_versions()[0]
        latest_version_str = [v for v in self.versions['available'] if get_exe_platform() in self.file_dict[v]][0]

        latest_version_tuple = tuple(map(int, latest_version_str.split(".")[:3]))
        current_version = sorted(local_versions)[-1]

        if latest_version_tuple[:level] > current_version[:level]:

            msg = "Current version: %s\nAvailable version: %s\n\n" % (
                current_version, latest_version_tuple)

            msg += "Do you want to download and install the update?"

            if self.ui.get_confirmation("Update", msg):
                self.update(latest_version_str)
                return True
            else:
                return False
        return True
