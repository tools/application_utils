'''
Created on 28. jul. 2017

@author: mmpe
'''
import importlib
import shutil
import sysconfig
import os
from datetime import datetime
import sys
import ssl
import certifi
from app_utils.functions.process_exec import pexec
from app_utils.build_exe.pyinstaller.build_pyinstaller_exe import build_pyinstaller_exe, build_from_spec, \
    make_spec
from app_utils.build_exe.launcher import pascal_launcher
from app_utils.utils import git_utils


def make_standard_spec(app_module,
                       build_args=[],
                       ):
    version = importlib.import_module('version', app_module.__package__)
    print("Building version:\t%s" % str(version.__version__))
    make_spec(app_module.__file__, onefile=False, build_args=build_args)


def make_standard_installer(
        app_module,
        build_args,
        PRETEST=0,
        BUILD_EXE=1,
        TEST=1,
        MAKE_ZIP=1,
        UPLOAD=1,
        DELETE_UNPACKED=0,
        COPY_SOURCE=0,
        UPLOAD_DIR=None):

    if PRETEST:
        print("Testing")
        return_code, stdout, stderr, _ = pexec(
            [sys.executable, app_module.__file__, "terminate"], cwd=os.path.dirname(app_module.__file__))
        if return_code != 0:
            print(return_code)
            print(stdout)
            print(stderr)
            assert return_code == 0
        print("Test succeeded")

    version = importlib.import_module(f'{app_module.__package__}.version').__version__
    platform = get_exe_platform()
    exe_ext = ("", '.exe')[os.name == 'nt']

    print("Building version:\t%s" % version)
    name = app_module.__name__.split(".")[-1]
    try:
        dist_path = "./dist/"
        full_name = "%s-%s-%s" % (name, version, platform)
        # dists_path = "./dists/%s/" % (full_name)
        if BUILD_EXE:
            print("Building executable")

            if os.path.isdir(dist_path):
                shutil.rmtree(dist_path)
            if os.path.isdir("./dists/%s/%s/" % (full_name, name)):
                shutil.rmtree("./dists/%s/%s/" % (full_name, name))
            if isinstance(build_args, str) and build_args.endswith('.spec'):
                build_from_spec(app_module.__file__, build_args, version)
            else:
                build_pyinstaller_exe(app_module.__file__,
                                      version, platform, onefile=False, build_args=build_args)
            print("Move to dists")
            print(dist_path, os.path.isdir(dist_path))
            print("./dists/%s/%s/" % (full_name, name), os.path.isdir("./dists/%s/%s/" % (full_name, name)))

            shutil.move(dist_path, "./dists/%s/%s/" % (full_name, name))

            shutil.copy(os.path.dirname(pascal_launcher.__file__) + "/launcher_%s" % platform,
                        "./dists/%s/%s/%s%s" % (full_name, name, name, exe_ext))
            with open("./dists/%s/%s/%s/qt.conf" % (full_name, name, full_name), 'w') as fid:
                fid.write("[Paths]\nPrefix = PyQt5/Qt")

            DigiCertUtil_path = r"C:\Program Files\DigiCertUtil\DigiCertUtil.exe"
            if os.path.isfile(DigiCertUtil_path):
                print("signing executables")
                exe_path = "./dists/%s/%s/%s/%s%s" % (full_name, name, full_name, name, exe_ext)
                launcher_path = "./dists/%s/%s/%s%s" % (full_name, name, name, exe_ext)
                res = os.system(f'"{DigiCertUtil_path}" sign /noInput {exe_path} {launcher_path}')
                if res == 0:
                    print("Executables signed successfully")

        if TEST:
            print("Testing")
            return_code, stdout, stderr, _ = (
                pexec(["./dists/%s/%s/%s/%s%s" % (full_name, name, full_name, name, exe_ext), "terminate"]))
            if 1:  # return_code != 0 or stderr.strip() != "":
                print(return_code)
                print(stdout)
                print(stderr)
            assert return_code == 0
            print("Test succeeded")

        from urllib.request import urlopen
        if version.split("-")[0].split(".")[-1] == "0":
            htm_name = 'index.htm'
        else:
            htm_name = 'dev.htm'

        if MAKE_ZIP:
            print("Making zip")
            shutil.make_archive("./dists/%s" % (full_name), 'zip', "./dists/%s/" % (full_name))
            shutil.move("./dists/%s.zip" % (full_name), "./dists/%s/%s.zip" % (full_name, full_name))

        if UPLOAD:
            if all([v.isnumeric() for v in version.split('.')]):
                print('Upload')
                if UPLOAD_DIR is None:
                    if os.name == 'posix':
                        UPLOAD_DIR = "/mnt/www_c/inetpub/wwwroot/tools/"
                    else:
                        UPLOAD_DIR = r'\\130.226.56.174\c$\inetpub\wwwroot\tools/'

                f_from = "./dists/%s/%s.zip" % (full_name, full_name)
                f_to = UPLOAD_DIR + "%s/downloads/%s/%s.zip" % (name, platform, full_name)
                print(f"upload, '{f_from}' to '{f_to}'")
                shutil.copy("./dists/%s/%s.zip" % (full_name, full_name),
                            UPLOAD_DIR + "%s/downloads/%s/%s.zip" % (name, platform, full_name))
                shutil.copy("./dists/%s/%s" % (full_name, htm_name), UPLOAD_DIR +
                            "%s/downloads/%s/%s" % (name, platform, htm_name))
            else:
                print("Not allowed to upload untagged versions")

    except Exception:

        raise
    print("Finish - new version:\t\t%s" % version)


def get_exe_platform():
    return sysconfig.get_platform().replace("amd", '').replace('x86', '').replace("-", '').replace("_", '')


def main():
    if __name__ == '__main__':
        from app_utils.tests.test_files import my_program

        make_standard_installer(my_program,
                                build_args=['console'],
                                PRETEST=1,
                                BUILD_EXE=1,
                                TEST=1,
                                MAKE_ZIP=0,
                                UPLOAD=0,
                                DELETE_UNPACKED=1,
                                COPY_SOURCE=0)


main()
