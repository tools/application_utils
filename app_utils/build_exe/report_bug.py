import requests as req
from enum import Enum


class Severity(Enum):
    Critical = 1
    High = 2
    Medium = 3
    Low = 4
    Info = 5
    Unknown = 6


def report_bug(url, auth_key, title, description, severity=Severity.High, **kwargs):
    payload = {"title": title,
               "description": description,
               "severity": severity.name,
               **kwargs}

    headers = {"Authorization": f"Bearer {auth_key}",
               "Content-Type": "application/json"}
    resp = req.post(url, json=payload, headers=headers)
    print(resp)
