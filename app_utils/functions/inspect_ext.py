import inspect
import os
import time
import zipfile
import types
import sys
from setuptools import find_packages
from pkgutil import iter_modules
import pkgutil


def class_list(func_path, base_class, exclude_classes=[]):
    exclude_classes.append(base_class)
    class_lst = []

    modules = find_modules(func_path, prefix='')

    for module_name in modules:
        t = time.time()
        try:
            module = __import__(module_name, {}, {}, ['*'])
            class_lst.extend([cls for cls in module.__dict__.values()
                              if inspect.isclass(cls) and issubclass(cls, base_class) and cls not in exclude_classes])
        except Exception as e:
            print(e)
            class_lst.append(type(e)("Failed to import %s\n" % module_name + str(e)).with_traceback(sys.exc_info()[2]))
            # tb = "".join(traceback.format_tb(sys.exc_info()[2])).replace(r"""File ".\%s""" % func_path, r"""File "%s""" % os.path.realpath(func_path))
            # class_lst.append(type(e)("Failed to import %s\n" % (tb, module_name) + str(e)))
        if time.time() - t > 1:
            print(module_name, time.time() - t)
    return class_lst


def _get_all_modules_pyinstaller(path):
    # Special handling for PyInstaller
    toc = set()

    importers = pkgutil.iter_importers(os.path.abspath(path))
    for i in importers:
        if hasattr(i, 'toc'):
            toc |= i.toc
    return toc


def find_modules(path, prefix):
    modules = set()

    def add_modules(pkg):
        for info in iter_modules([pkg.replace('.', '/')]):
            if not info.ispkg:
                modules.add(pkg + '.' + info.name)

    add_modules(path.replace('/', '.'))
    for pkg in find_packages(path):
        pkg = path.replace('/', '.') + '.' + pkg
        modules.add(pkg)
        add_modules(pkg)

    if hasattr(sys, "frozen"):
        frozen_prefix = path.replace("/", '.')
        modules |= {m for m in _get_all_modules_pyinstaller(path) if m.startswith(frozen_prefix)}
    return sorted([m for m in modules if m.startswith(prefix)])


def argument_string(func):
    if hasattr(func, 'target_function'):
        func = func.target_function
    try:
        args, varargs, keywords, defaults = inspect.getfullargspec(func)[:4]
        if defaults is not None:
            for nr in range(1, len(defaults) + 1):
                d = defaults[-nr]
                if isinstance(d, str):
                    d = "'%s'" % d
                elif isinstance(d, types.FunctionType) and hasattr(d, "__name__"):
                    if d.__module__.startswith('numpy.'):
                        d = "np.%s" % d.__name__
                    else:
                        d = d.__name__
                elif isinstance(d, type):
                    d = str(d).split("'")[1].replace("numpy", "np")
                args[-nr] = "%s=%s" % (args[-nr], d)

        if varargs is not None:
            args.append("*%s" % varargs)

        if keywords is not None:
            args.append("**%s" % keywords)

        if len(args) > 0 and args[0] == 'self':
            return "(%s)" % ", ".join(args[1:])  # remove self
        else:
            return "(%s)" % ", ".join(args)
    except TypeError:
        return ""
