def singleton(cls):
    instances = {}

    def getinstance(*args, **kwargs):
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        return instances[cls]
    return getinstance


if __name__ == '__main__':
    @singleton
    class A(object):
        pass

    a1 = A()
    a1.var = 1
    a2 = A()
    print(a2.var)
