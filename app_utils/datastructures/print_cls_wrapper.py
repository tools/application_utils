# '''
# Created on 21. feb. 2018
#
# @author: mmpe
# '''
# import sys
# import traceback
#
#
# class ClassWrapperPrintCalls(object):
#
#     def __init__(self, cls, *args, **kwargs):
#         self.obj = cls(*args, **kwargs)
#
#     def __getattr__(self, name):
#
#         f = getattr(self.obj, name)
#         if callable(f):
#             def print_f(*args, **kwargs):
#                 print("Call ", f.__name__)
#                 return f(*args, **kwargs)
#             return print_f
#         else:
#             return f
#
#
# class ClassWrapperErrorHandleCalls(object):
#
#     def __init__(self, cls, *args, **kwargs):
#         self.obj = cls(*args, **kwargs)
#
#     def __getattr__(self, name):
#
#         f = getattr(self.obj, name)
#         if callable(f):
#             def print_f(*args, **kwargs):
#                 try:
#                     return f(*args, **kwargs)
#                 except:
#                     sys.stderr.write(traceback.format_exc())
#             return print_f
#         else:
#             return f
#
#
# if __name__ == '__main__':
#
#     class A(object):
#
#         def __init__(self, a, b=5):
#             self.a = a
#             self.b = b
#
#         def c(self):
#             return 3
#
#         def d(self):
#             1 / 0
#     a = ClassWrapperPrintCalls(A, 1, 2)
#     print(a.a)
#     print(a.b)
#     print(a.c())
#
#     a = ClassWrapperErrorHandleCalls(A, 1, 2)
#     a.d()
