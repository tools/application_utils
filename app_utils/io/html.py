'''
Created on 8. feb. 2017

@author: mmpe
'''
from app_utils.ui.text_ui import TextUI
import os
import urllib.request
import ssl
import certifi


class HTTP(object):

    def __init__(self, ui=TextUI()):
        self.ui = ui

    def read(self, url, encoding='utf-8'):
        os.environ['SSL_CERT_FILE'] = certifi.where()
        with urllib.request.urlopen(url, context=ssl.create_default_context(cafile=certifi.where())) as response:
            return response.read().decode(encoding)

    def download(self, source, destination):
        os.makedirs(os.path.dirname(destination), exist_ok=True)
        if os.path.isdir(destination):
            destination = os.path.join(destination, os.path.basename(source))

        u = urllib.request.urlopen(source)
        file_size = int(u.info().get_all("Content-Length")[0])

        block_sz = 8192
        with open(destination, 'wb') as f:
            for _ in self.ui.progress_iterator(
                    range(file_size // 8192 + 1), text="Downloading: %s MB: %.1f" % (os.path.basename(destination), file_size / 1024 / 1024)):
                buffer = u.read(block_sz)
                if not buffer:
                    break
                f.write(buffer)
            else:
                while True:
                    buffer = u.read(block_sz)
                    if not buffer:
                        break
                    f.write(buffer)
        return destination
